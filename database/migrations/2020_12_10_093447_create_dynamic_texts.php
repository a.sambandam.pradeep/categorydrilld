<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDynamicTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_texts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('tooltip1')->nullable();
            $table->longText('tooltip2')->nullable();
            $table->longText('tooltip3')->nullable();
            $table->longText('tooltip4')->nullable();
            $table->longText('tooltip5')->nullable();
            $table->longText('tooltip6')->nullable();
            $table->longText('tooltip7')->nullable();
            $table->longText('tooltip8')->nullable();
            $table->longText('tooltip9')->nullable();
            $table->longText('tooltip10')->nullable();
            $table->longText('tooltip11')->nullable();
            $table->longText('tooltip12')->nullable();
            $table->longText('tooltip13')->nullable();
            $table->longText('tooltip14')->nullable();
            $table->longText('tooltip15')->nullable();
            $table->longText('tooltip16')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_texts');
    }
}
