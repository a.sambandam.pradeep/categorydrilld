
@extends('layouts/fullLayoutMaster')

@section('title', 'Data Form')
@section('vendor-style')
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
        <link href="https://cdn.quilljs.com/1.0.0/quill.snow.css" rel="stylesheet">
@endsection
@section('content')

<style>
    .app-content.content{
        background: transparent linear-gradient(180deg, #8E2DE2 0%, #4A00E0 100%) 0% 0% no-repeat padding-box;
		min-height: 100vh;
    }
	
    </style>
<!-- Basic Vertical form layout section start -->
<section id="basic-vertical-layouts">
  <div class="row match-height">
      
      <div class="col-md-8 offset-md-2 col-12">
          <div class="card">
              <div class="card-content">
                  <div class="card-body">
                    <h4 class="text-bold-800 color-dblack font-medium-4">בוקר טוב, {{ $user->name }}</h4>@if($user->name != 'Eyal')<a href="/"><i class="feather icon-arrow-left"></i> Back</a> | @endif<a href="auth-logout"><i class="feather icon-power"></i> Logout</a> 
                    <hr />
                    <p class="text-bold-600 color-dblack font-medium-4">עדכון תוכן</p>     
                    <div class="accordion" id="accordionExample0">
                      <div class="collapse-border-item collapse-header card collapse-bordered">
                        <div class="card-header" id="heading190" data-toggle="collapse" role="button" data-target="#collapse190" aria-expanded="false" aria-controls="collapse190">
                          <span class="lead collapse-title">Insights</span>
                        </div>
                        <div id="collapse190" class="collapse" aria-labelledby="heading190" data-parent="#accordionExample0">
                          <div class="card-body">
                            <button type="button" data-toggle="modal" data-backdrop="static" data-target="#insightmodal" id="newbtn2" class="btn btn-sm btn-primary mr-1 mb-1 waves-effect waves-light font-medium-1">Add Insight</button>
                            <div class="table-responsive">
                              <table class="table table-bordered table-striped">
                                <thead>
                                  <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Icon</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Brand</th>
                                    <th scope="col">Actions</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($insights as $insight)
                                    <tr class="item{{$insight->id}}" >
                                      <td>{{$insight->title}}</td>
                                      <td><img src="{{asset('uploads/'.$insight->icon) }}" alt="" width="30px" /></td>
                                      <td>{{$insight->category}}</td>
                                      <td>{{$insight->brand}}</td>
                                      <td>
                                        <i class="fa fa-edit editClassIns" data-info="{{$insight->id}}|,{{$insight->title}}|,{{$insight->category}}|,{{$insight->brand}}|,{{$insight->maintext}}|,{{$insight->datetext}}|,{{$insight->showin}}"></i>&nbsp;
                                        <i class="fa fa-trash deleteClassIns" data-info="{{$insight->id}}"></i>
                                      </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>                        
                        <div class="card-header" id="heading200" data-toggle="collapse" role="button" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200" style="display:none;">
                          <span class="lead collapse-title">המלצות</span>
                        </div>
                        <div id="collapse200" class="collapse" aria-labelledby="heading200" data-parent="#accordionExample0">
                          <div class="card-body">
                            <button type="button" data-toggle="modal" data-backdrop="static" data-target="#recommendation1modal" id="newbtn" class="btn btn-sm btn-primary mr-1 mb-1 waves-effect waves-light font-medium-1">הוסף מלצות</button>
                              <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                  <thead>
                                    <tr>
                                      <th scope="col">טקסט המלצות</th>
                                      <th scope="col">טקסט כפתור</th>
                                      <th scope="col">קטגוריה</th>
                                      <th scope="col">אייקון</th>
                                      <th scope="col">פעולה</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($rows as $item)
                                      <tr class="item{{$item->id}}" >
                                        <td>{{$item->recommendation}}</td>
                                        <td>{{$item->recommendationButton}}</td>
                                        <td>{{$item->category}}</td>
                                        <td>{{$item->icon}}</td>
                                        <td>
                                          <i class="fa fa-edit editClass" data-info="{{$item->id}}|,{{$item->recommendation}}|,{{$item->recommendationButton}}|,{{$item->notes}}|,{{$item->category}}|,{{$item->icon}}"></i>&nbsp;
                                          <i class="fa fa-trash deleteClass" data-info="{{$item->id}}"></i>
                                        </td>
                                      </tr>
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
  <div class="collapse-border-item card collapse-bordered">
  <div class="card-header" id="heading210" data-toggle="collapse" role="button" data-target="#collapse210"
                        aria-expanded="false" aria-controls="collapse210">
                      <span class="lead collapse-title">
                        הגדרת תיאור
                      </span>
                    </div>
                    <div id="collapse210" class="collapse" aria-labelledby="heading210" data-parent="#accordionExample0">
      <div class="card-body">
      <form class="form form-vertical" action="{{ url('update-tooltip')}}" method="POST" name="recommendation">
      <div class="col-12 form-group row">

                                  @for ($i = 1; $i <= 16; $i++) 
                                    <fieldset class="col-4 mb-3">                                            
                                            <div class="font-medium-2 "> {{ $tttextarr[$i-1] }} </div>
                                            <input type='text' class="form-control" name="tooltip{{$i}}" id="tooltip{{$i}}" placeholder="{{ $tttextarr[$i-1] }}" value="{{$request['tooltip'.$i]}}"/>

                                        </fieldset>
                                   @endfor 
                                  </div>

                         </div>
                         	{{ csrf_field() }}
                             <input type="hidden" id="post_type" name="post_type" value="tooltips"/>
                             <div class="px-1 py-1">
                             <button type="submit" class="btn btn-primary">Submit</button>
  </div>
                      </form>
      </div>
    </div>
  </div>
  
</div>          
                    
                          
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
<!-- // Basic Vertical form layout section end -->

<!-- Recommendation Modal 1 -->
<div class="modal fade" id="recommendation1modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">הוסף / ערוך המלצות</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form class="form form-vertical" action="{{ url('update-text')}}" method="POST" id="rcform" name="recommendation">
                          <div class="form-body ">
                          <div class="row form-group">
                                    <fieldset class="col-6 mb-1">
                                        <div class="font-medium-2"> טקסט המלצות </div>
                                        <input type='text' class="form-control form-control-lg" name="recommendation1" id="recommendation1" placeholder="טקסט המלצות" value="{{$request['recommendation1']}}"/>
                                        </fieldset>
                                        <fieldset class="col-6 mb-1">
                                            <div class="font-medium-2">טקסט כפתור</div>
                                            <input type='text' class="form-control form-control-lg" name="recommendationButton1" id="recommendationButton1" placeholder="טקסט כפתור" value="{{$request['recommendationButton1']}}"/>
                                        </fieldset>
                                        <fieldset class="col-6 mb-1">
                                            <div class=" font-medium-2 ">קטגוריה</div>
											<select name="category" id="category" class="form-control form-control-lg">
												 <option value="">בחר קטגוריה</option>
												 @foreach($categories as $category)
												 @if( $request['category'] == $category->category)
														<option value="{{ $category->category}}" selected>{{ $category->category }}</option>
													@else
														<option value="{{ $category->category }}">{{ $category->category}}</option>
													@endif	
												 @endforeach
											</select>
                                        </fieldset>
                                        <fieldset class="col-6 mb-1">
                                          
                                                <div class="font-medium-2">אייקון</div>
											<select name="icon" id="icon" class="form-control form-control-lg">
												 <option value="" selected>בחר אייקון</option>
												 <option value="Red" 
                                                 @if ($request['icon']=='Red')
                                                    selected="selected"
                                                 @endif
                                                >Red</option>
                                                 <option value="Green"
                                                 @if ($request['icon']=='Green')
                                                    selected="selected"
                                                 @endif
                                                 >Green</option>
											</select>
                                        </fieldset>
</div>
<div class="row form-group">
                                        <fieldset class="px-1 col-12"> 
                                            <div class="font-medium-2  pt-1">Notes</div>
                                            <input name="notes1" id ="notes1" type="hidden" value="{{$request['notes1']}}">
                                            <!-- Create the editor container -->
                                            <div id="editor" data-notes="{{$request['notes1']}}">
                                            </div>
                                        </fieldset>
                                  </div>
                                  {{ csrf_field() }}
                                  <input type="hidden" id="rec_id" name="rec_id" value="{{$request['id']}}"/>
                             <input type="hidden" id="post_type" name="post_type" value="recommendation"/>
                             <div class="px-1 pt-2">
                             <button type="submit" class="btn btn-primary submitButton">{{$request['buttonText']}}</button>
</div>
                                 </form>
            </div>
        </div>
    </div>
</div>
  </div>
<!-- Recommendation Modal 1 -->


<!-- Insight Modal 1 -->
<div class="modal fade" id="insightmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add/Edit Insight</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form class="form form-vertical" action="{{ url('update-text')}}" method="POST" id="igform" name="insight" enctype="multipart/form-data">
              <div class="form-body ">
                <div class="row form-group">
                  <fieldset class="col-6 mb-1">
                    <div class="font-medium-2">Insight Title</div>
                    <input type='text' class="form-control form-control-lg" name="title" id="title" placeholder="Insight Title" value=""/>
                  </fieldset>
                  <fieldset class="col-6 mb-1">
                    <div class="font-medium-2">Insight Icon</div>
                    <input type='file' class="form-control form-control-lg" name="icon" id="icon"/>
                  </fieldset>
                </div>
                
                <div class="row form-group">
                      <fieldset class="px-1 col-12"> 
                          <div class="font-medium-2  pt-1">Insight Main Text</div>
                          <input name="maintext" id ="maintext" type="hidden" value="">
                          <!-- Create the editor container -->
                          <div id="editor2" data-notes="" style="height:100px;">
                          </div>
                      </fieldset>
                </div>
                <div class="row form-group">
                  <fieldset class="col-12 mb-1">
                    <div class="font-medium-2">Insight Date Text</div>
                    <input type='text' class="form-control form-control-lg" name="datetext" id="datetext" placeholder="Insight Date Text" value=""/>
                  </fieldset>
                </div>
                <div class="row form-group">
                  <fieldset class="col-6 mb-1">
                    <div class="font-medium-2">Insight Category</div>
                    <select class="form-control form-control-lg" name="category" id="inscategory">
                      <option value="">Choose a category</option>
                      <option value="טונה">טונה</option>
                    </select>
                  </fieldset>
                  <fieldset class="col-6 mb-1">
                    <div class="font-medium-2">Insight Brand</div>
                    <select class="form-control form-control-lg" name="brand" id="insbrand">
                      <option value="">Choose a brand (optional)</option>
                      <option value="פיל - טונה">פיל - טונה</option>
                      <option value="סטארקיסט">סטארקיסט</option>
                      <option value="פוסידון">פוסידון</option>
                      <option value="וילי פוד">וילי פוד</option>
                      <option value="ויליגר">ויליגר</option>
                      <option value="אחר">אחר</option>
                    </select>
                  </fieldset>
                </div>
                <div class="row form-group">
                  <fieldset class="col-12 mb-1">
                    <div class="font-medium-2">Show in</div>
                    <input type="checkbox" name="showin[]" id="Buyers" value="Buyers" /> Buyers<br />
                    <input type="checkbox" name="showin[]" id="Funnel" value="Funnel" /> Funnel<br />
                    <input type="checkbox" name="showin[]" id="Sales" value="Sales" /> Sales<br />
                  </fieldset>
                </div>
                {{ csrf_field() }}
                <input type="hidden" id="ins_id" name="ins_id" value=""/>
                <input type="hidden" id="post_type" name="post_type" value="insight"/>
                <div class="px-1 pt-2">
                  <button type="submit" class="btn btn-primary submitButton">Submit</button>
                </div>
              </div>
            </form>


            </div>
        </div>
    </div>
</div>
<!-- Insight Modal 1 -->

@endsection

@section('vendor-script')
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
        <!-- Include the Quill library -->
<script src="https://cdn.quilljs.com/1.0.0/quill.js"></script>
 
 <!-- Initialize Quill editor -->
 <script>
   var editor = new Quill('#editor', {
  modules: {
    toolbar: [
      ['bold', 'italic'],
      ['link', 'blockquote', 'code-block', 'image'],
      [{ list: 'ordered' }, { list: 'bullet' }]
    ]
  },
  placeholder: '',
  theme: 'snow'
});
editor.format('direction', 'rtl');
editor.format('align', 'right');
   editor.setContents($(this).data['notes'], 'api');
   var form = document.getElementById('rcform');
   var myEditor = document.querySelector('#editor')
   var notes1 = document.querySelector('input[name=notes1]');
   var html = notes1.value; 
   myEditor.children[0].innerHTML = html;
   form.onsubmit = function(e) {
  // Populate hidden form on submit
  var notes1 = document.querySelector('input[name=notes1]');
  var myEditor = document.querySelector('#editor')
  var html = myEditor.children[0].innerHTML
  notes1.value =  html;
  return; 
    };


    var editor2 = new Quill('#editor2', {
  modules: {
    toolbar: [
      ['bold', 'italic'],
      ['link', 'blockquote', 'code-block', 'image'],
      [{ list: 'ordered' }, { list: 'bullet' }]
    ]
  },
  placeholder: '',
  theme: 'snow'
});
editor2.format('direction', 'rtl');
editor2.format('align', 'right');
//editor2.setContents($(this).data['notes'], 'api');
   var form2 = document.getElementById('igform');
   var myEditor = document.querySelector('#editor2')
   var maintext = document.querySelector('input[name=maintext]');
   var html = maintext.value; 
   myEditor.children[0].innerHTML = html;
   form2.onsubmit = function(e) {
  // Populate hidden form on submit
  var notes1 = document.querySelector('input[name=maintext]');
  var myEditor = document.querySelector('#editor2')
  var html = myEditor.children[0].innerHTML
  notes1.value =  html;
  return; 
    };



    $('.editClass').click(function() {
        var stuff = $(this).data('info').split('|,');
        $('#rec_id').val(stuff[0]);
        $('#recommendation1').val(stuff[1]);
        $('#recommendationButton1').val(stuff[2]);
        var myEditor = document.querySelector('#editor')
        var notes1 = document.querySelector('input[name=notes1]');
        notes1.value = stuff[3]; 
        myEditor.children[0].innerHTML = stuff[3];
        $('#category').val(stuff[4]);
        $('#icon').val(stuff[5]);
        //$('.submitButton').text('Update');
        $('#newbtn').trigger('click');
    });

    $('.deleteClass').click(function() {
        var id = $(this).data('info');
        var _token = $('input[name="_token"]').val();
        $.ajax({
            type: 'post',
            url: '/delete-text',
            data: {
                _token:_token,
                id: id
            },
            success: function(data) {
                window.location.reload();
            }
        });
        console.log(id);
    });

    
    $('.editClassIns').click(function() {
        var stuff = $(this).data('info').split('|,');
        $('#ins_id').val(stuff[0]);
        $('#title').val(stuff[1]);
        $('#inscategory').val(stuff[2]);
        $('#insbrand').val(stuff[3]);

        //$('#maintext').val(stuff[4]);
        var myEditor = document.querySelector('#editor2')
        var notes1 = document.querySelector('input[name=maintext]');
        notes1.value = stuff[4]; 
        myEditor.children[0].innerHTML = stuff[4];

        $('#datetext').val(stuff[5]);
        var showinarr = stuff[6].split(',');
        if(stuff[6] != ''){
          showinarr.forEach(element => $('#'+element).prop('checked',true));
        }

        $('#newbtn2').trigger('click');
    });

    
    $('.deleteClassIns').click(function() {
        var id = $(this).data('info');
        var _token = $('input[name="_token"]').val();
        $.ajax({
            type: 'post',
            url: '/delete-insight',
            data: {
                _token:_token,
                id: id
            },
            success: function(data) {
                window.location.reload();
            }
        });
        console.log(id);
    });
    
 </script> 
 
@endsection
@section('page-script')
        <!-- Page js files -->
        <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>
@endsection
