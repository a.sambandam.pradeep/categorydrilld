
@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
		<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
		@if($noresults != 0)
		<link rel="stylesheet" href="{{ asset(mix('css/pages/error.css')) }}">
		@endif
        <link rel="stylesheet" href="{{ asset(mix('css/pages/dashboard-ecommerce.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/card-analytics.css')) }}">
@endsection
@section('content')
  {{-- Dashboard Ecommerce Starts --}}
  @if($noresults == 0)
  <section id="dashboard-ecommerce">
	<div class="row d-flex justify-content-between align-items-end pad-bottom20">
		<div class="col-md-1 col-6">
			<h3 class="text-bold-200 color-dblack">Buyer Profile</h3>
		</div>
		<div class="col-md-11 col-6">
			<hr />
		</div>
	</div>
    <div class="row match-height firstrow">
        <div class="col-lg-3 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="text-bold-800 color-dblack">Average Age</h4>
                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[0] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                </div>
                <hr class="m-10 mb-2" />
                <div class="card-content">
                    <div class="card-body pt-0 row d-flex align-items-center">
                        <div id="chart1" class="col-md-6 col-12 doughcharts mb-1" data-labels="18 - 25,25 - 35,35 - 45,45 - 55,55 - 65,65+" data-percentages="{{ $agePercentages }}" data-colors="#5A30DC,#9013FE,#DAB0E8,#b561cf,#DA4CB2,#EB87CF"data-totalval="{{ $productavgage }}" data-labelcolors="#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A"></div>
                        <div class="col-md-3 col-6">
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color1"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">18 - 25</span>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color2"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">25 - 35</span>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color3"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">35 - 45</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-6">
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color4"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">45 - 55</span>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color5"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">55 - 65</span>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color6"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">65+</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="m-10" />
                <div class="card-content">
                    <div class="card-body pt-0 row d-flex align-items-center">
                        <div class="series-result">
							@if($productavgage > $categoryavgage)
								<i class="feather icon-arrow-up color-green"></i>
								<span>{{ $differenceage }} years older than the category average</span>
							@else
								<i class="feather icon-arrow-down color-orange"></i>
								<span>{{ $differenceage }} years younger than the category average</span>
							@endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="text-bold-800 color-dblack">Gender Segmentation</h4>
                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[1] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                </div>
                <hr class="m-10 mb-2" />
                <div class="card-content">
                    <div class="card-body pt-0 row d-flex align-items-center">
                        <div id="chart2" class="col-md-6 col-12 doughcharts mb-1" data-labels="נשים,גברים" data-icon="f007" data-percentages="{{ $genderPercentages }}" data-colors="#19ABDE,#186BA1" data-labelcolors="#4A4A4A,#4A4A4A"></div>
                        <div class="col-md-3 col-6">
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color7"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">Women</span>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color8"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">Men</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">

                        </div>
                    </div>
                </div>
                <hr class="m-10" />
                <div class="card-content">
                    <div class="card-body pt-0 row d-flex align-items-center">
                        <div class="series-result">
							@if($productMalePercent > $categoryMalePercent)
								<i class="feather icon-arrow-up color-green"></i>
								<span>‏{{ $productMalePercent-$categoryMalePercent }}% more men than average in category</span><br />
							@else
								<i class="feather icon-arrow-down color-orange"></i>
								<span>{{ $categoryMalePercent-$productMalePercent }}% fewer men than the average in the category</span><br />
							@endif

							@if($productFemalePercent > $categoryFemalePercent)
								<i class="feather icon-arrow-up color-green"></i>
								<span>‏&rlm;{{ $productFemalePercent-$categoryFemalePercent }}% more women than average in the category</span>
							@else
								<i class="feather icon-arrow-down color-orange"></i>
								<span>&rlm;{{ $categoryFemalePercent-$productFemalePercent }}% fewer women than average in the category</span>
							@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Buyers Personality -->
        <div class="col-lg-3 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="text-bold-800 color-dblack">Type of purchase</h4>
                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true"  title="{{ $tootltips[2] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                </div>
                <hr class="m-10 mb-2" />
                <div class="card-content">
                    <div class="card-body pt-0 row d-flex align-items-center">
                        <div id="chart3" class="col-md-6 col-12 doughcharts mb-1" data-labels="אמוציונלי,רציונלי" data-icon="f004" data-percentages="{{ $emotional_buyers }},{{$rational_buyers}}" data-colors="#DA4CB2,#AF4BCF" data-labelcolors="#4A4A4A,#4A4A4A"></div>
                        <div class="col-md-3 col-6">
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color9"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">Emotional</span>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color10"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">Rational</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">

                        </div>
                    </div>
                </div>
                <hr class="m-10" />
                <div class="card-content">
                    <div class="card-body pt-0 row d-flex align-items-center">
                        <div class="series-result">
                            @if($productRPercent > $categoryRPercent)
								<i class="feather icon-arrow-up color-green"></i>
								<span>‏{{ $productRPercent-$categoryRPercent }}% more Rational than average in the category</span><br />
							@else
								<i class="feather icon-arrow-down color-orange"></i>
								<span>{{ $categoryRPercent-$productRPercent }}% fewer Rational than average in the category</span><br />
							@endif

							@if($productEPercent > $categoryEPercent)
								<i class="feather icon-arrow-up color-green"></i>
								<span>‏&rlm;{{ $productEPercent-$categoryEPercent }}% more Emotional than average in the category</span>
							@else
								<i class="feather icon-arrow-down color-orange"></i>
								<span>&rlm;{{ $categoryEPercent-$productEPercent }}% fewer Emotional than average in the category</span>
							@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="text-bold-800 color-dblack">Family Composition</h4>
                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[3] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                </div>
                <hr class="m-10 mb-2" />
                <div class="card-content">
                    <div class="card-body pt-0 row d-flex align-items-center">
                        <div id="chart4" class="col-md-6 col-12 doughcharts mb-1" data-labels="אדם בודד,משפחות,זוגות" data-icon="f0c0" data-percentages="{{ $compositionPercentages }}" data-colors="#EABE3B,#EF7E32,#DE542D" data-labelcolors="#4A4A4A,#4A4A4A,#4A4A4A"></div>
                        <div class="col-6">
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color11"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">Single</span>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color12"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">Families</span>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between mb-1">
                                <div class="series-info d-flex align-items-center">
                                    <i class="fa fa-circle font-medium-2 text-color13"></i>
                                    <span class="text-bold-600 mx-50 color-lblack">Couples</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="m-10" />
                <div class="card-content">
                    <div class="card-body pt-0 row d-flex align-items-center">
                        <div class="series-result">
							@if($productSinglePercent > $categorySinglePercent)
								<i class="feather icon-arrow-up color-green"></i>
								<span>‏{{ $productSinglePercent-$categorySinglePercent }}% more Single person than the average in the category</span><br />
							@else
								<i class="feather icon-arrow-down color-orange"></i>
								<span>{{ $categorySinglePercent-$productSinglePercent }}% fewer Single person than the average in the category</span><br />
							@endif

							@if($productFamilyPercent > $categoryFamilyPercent)
								<i class="feather icon-arrow-up color-green"></i>
								<span>‏&rlm;{{ $productFamilyPercent-$categoryFamilyPercent }}% more Families than average in the category</span><br />
							@else
								<i class="feather icon-arrow-down color-orange"></i>
								<span>&rlm;{{ $categoryFamilyPercent-$productFamilyPercent }}% fewer Families than average in the category</span> <br />
							@endif

							@if($productCouplePercent > $categoryCouplePercent)
								<i class="feather icon-arrow-up color-green"></i>
								<span>‏&rlm;{{ $productCouplePercent-$categoryCouplePercent }}% more Couples than average in the category</span>
							@else
								<i class="feather icon-arrow-down color-orange"></i>
								<span>&rlm;{{ $categoryCouplePercent-$productCouplePercent }}% more Couples than average in the category</span>
							@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row d-flex justify-content-between align-items-end pad-bottom20">
        <div class="col-md-1 col-6">
            <h3 class="text-bold-200 color-dblack">פרופיל חנות</h3>
        </div>
        <div class="col-md-11 col-6">
            <hr />
        </div>
    </div>
    <div class="row secondrow">
        <div class="col-lg-5 col-12">
            <div class="card mb-1 col-12">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="text-bold-800 color-dblack">מעורבות קהל בחנות</h4>
                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[4] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                </div>
                <hr class="m-10 mb-2" />
                <div class="card-content">
                    <div class="card-body pt-0">
                        <div class="features row mb-1">
                            <div class="col-md-4 col-12">
                                <span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[5] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>
                                <span class="font-medium-1 text-bold-800 color-dblack">אינטרקציות במחלקה</span>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="progress bg-pbar progress-xl">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $dpInteractionPercentage }}" aria-valuemin="{{ $dpInteractionPercentage }}" aria-valuemax="100" style="width:{{ $dpInteractionPercentage }}%">{{ $dpInteractionPercentage }}%</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                @if($dpInteractionPercentage > $totalDpCartPercentage)
                                    <i class="feather icon-arrow-up color-green"></i>
                                    <span class="font-medium-1 text-bold-600 color-green">‏{{ $dpInteractionPercentage-$totalDpCartPercentage }}% מעל הממוצע</span><br />
                                @else
                                    <i class="feather icon-arrow-down color-orange"></i>
                                    <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$dpInteractionPercentage }}% מתחת לממוצע</span><br />
                                @endif
                            </div>
                        </div>
                        <div class="features row mb-1">
                            <div class="col-md-4 col-12">
                                <span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[6] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>
                                <span class="font-medium-1 text-bold-800 color-dblack">אינטרקציות עם הקטגוריה</span>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="progress bg-pbar progress-xl">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $categoryInteractionPercentage }}" aria-valuemin="{{ $categoryInteractionPercentage }}" aria-valuemax="100" style="width:{{ $categoryInteractionPercentage }}%">{{ $categoryInteractionPercentage }}%</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                @if($categoryInteractionPercentage > $totalDpCartPercentage)
                                    <i class="feather icon-arrow-up color-green"></i>
                                    <span class="font-medium-1 text-bold-600 color-green">‏{{ $categoryInteractionPercentage-$totalDpCartPercentage }}% מעל הממוצע</span><br />
                                @else
                                    <i class="feather icon-arrow-down color-orange"></i>
                                    <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$categoryInteractionPercentage }}% מתחת לממוצע</span><br />
                                @endif
                            </div>
                        </div>
                        <div class="features row mb-1">
                            <div class="col-md-4 col-12">
                                <span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[7] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>
                                <span class="font-medium-1 text-bold-800 color-dblack">‏אינטרקציה עם המותג</span>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="progress bg-pbar progress-xl">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $brandInteractionPercentage }}" aria-valuemin="{{ $brandInteractionPercentage }}" aria-valuemax="100" style="width:{{ $brandInteractionPercentage }}%">{{ $brandInteractionPercentage }}%</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                @if($brandInteractionPercentage > $totalDpCartPercentage)
                                    <i class="feather icon-arrow-up color-green"></i>
                                    <span class="font-medium-1 text-bold-600 color-green">‏{{ $brandInteractionPercentage-$totalDpCartPercentage }}% מעל הממוצע</span><br />
                                @else
                                    <i class="feather icon-arrow-down color-orange"></i>
                                    <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$brandInteractionPercentage }}% מתחת לממוצע</span><br />
                                @endif
                            </div>
                        </div>
                        <div class="features row mb-1">
                            <div class="col-md-4 col-12">
                                <span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[8] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>
                                <span class="font-medium-1 text-bold-800 color-dblack">אינטרקציה עם המוצר</span>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="progress bg-pbar progress-xl">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $productInteractionPercentage }}" aria-valuemin="{{ $productInteractionPercentage }}" aria-valuemax="100" style="width:{{ $productInteractionPercentage }}%">{{ $productInteractionPercentage }}%</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                @if($productInteractionPercentage > $totalDpCartPercentage)
                                    <i class="feather icon-arrow-up color-green"></i>
                                    <span class="font-medium-1 text-bold-600 color-green">‏{{ $productInteractionPercentage-$totalDpCartPercentage }}% מעל הממוצע</span><br />
                                @else
                                    <i class="feather icon-arrow-down color-orange"></i>
                                    <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$productInteractionPercentage }}% מתחת לממוצע</span><br />
                                @endif
                            </div>
                        </div>
                        <div class="features row mb-1">
                            <div class="col-md-4 col-12">
                                <span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[9] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>
                                <span class="font-medium-1 text-bold-800 color-dblack">‏רכישת מוצר</span>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="progress bg-pbar progress-xl">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $productInteractionPercentage }}" aria-valuemin="{{ $productInteractionPercentage }}" aria-valuemax="100" style="width:{{ $productInteractionPercentage }}%">{{ $productInteractionPercentage }}%</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                @if($productInteractionPercentage > $totalDpCartPercentage)
                                    <i class="feather icon-arrow-up color-green"></i>
                                    <span class="font-medium-1 text-bold-600 color-green">‏{{ $productInteractionPercentage-$totalDpCartPercentage }}% מעל הממוצע</span><br />
                                @else
                                    <i class="feather icon-arrow-down color-orange"></i>
                                    <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$productInteractionPercentage }}% מתחת לממוצע</span><br />
                                @endif
                            </div>
                        </div>
                        <div class="features row mb-1">
                            <div class="col-md-4 col-12">
                                <span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[10] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>
                                <span class="font-medium-1 text-bold-800 color-dblack">נטישה במהלך רכישה</span>
                            </div>
                            <div class="col-md-4 col-12">
                                <div class="progress bg-pbar progress-xl">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{ $productAbandonPercentage }}" aria-valuemin="{{ $productAbandonPercentage }}" aria-valuemax="100" style="width:{{ $productAbandonPercentage }}%">{{ $productAbandonPercentage }}%</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                @if($productAbandonPercentage > $totalDpCartPercentage)
                                    <i class="feather icon-arrow-up color-green"></i>
                                    <span class="font-medium-1 text-bold-600 color-green">‏{{ $productAbandonPercentage-$totalDpCartPercentage }}% מעל הממוצע</span><br />
                                @else
                                    <i class="feather icon-arrow-down color-orange"></i>
                                    <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$productAbandonPercentage }}% מתחת לממוצע</span><br />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end twcolrow">
                <div class="card col-12 col-md-6 modified">
                    <div class="card-header d-flex justify-content-between align-items-center pad-rl0">
                        <h4 class="text-bold-800 color-dblack"><span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[11] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>  נטישה למוצר</h4>
                        <p class="font-medium-1 mb-0 text-bold-400 color-dblack">{{ $product }}</p>
                    </div>
                    <hr class="m-10 mb-2" />
                    <div class="card-content">
                        <div class="card-body pt-0">
                            <div class="features row">
                                <div class="col-12">
                                    <div class="progress bg-orange-pbar progress-xl">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ $productAbandonPercentage }}" aria-valuemin="{{ $productAbandonPercentage }}" aria-valuemax="100" style="width:{{ $productAbandonPercentage }}%">{{ $productAbandonPercentage }}%</div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    @if($productAbandonPercentage > $totalDpCartPercentage)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span class="font-medium-1 text-bold-600 color-green">‏{{ $productAbandonPercentage-$totalDpCartPercentage }}% מעל הממוצע</span><br />
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$productAbandonPercentage }}% מתחת לממוצע</span><br />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card col-12 col-md-6 modified">
                    <div class="card-header d-flex justify-content-between align-items-center pad-rl0">
                        <h4 class="text-bold-800 color-dblack"><span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[12] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>  יחס ההמרה מהקטגוריה</h4>
                        <p class="font-medium-1 mb-0 text-bold-400 color-dblack">{{ $category }}</p>
                    </div>
                    <hr class="m-10 mb-2" />
                    <div class="card-content">
                        <div class="card-body pt-0">
                            <div class="features row">
                                <div class="col-12">
                                    <div class="progress bg-blue-pbar progress-xl">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ $categoryInteractionPercentage }}" aria-valuemin="{{ $categoryInteractionPercentage }}" aria-valuemax="100" style="width:{{ $categoryInteractionPercentage }}%">{{ $categoryInteractionPercentage }}%</div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    @if($categoryInteractionPercentage > $totalDpCartPercentage)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span class="font-medium-1 text-bold-600 color-green">‏{{ $categoryInteractionPercentage-$totalDpCartPercentage }}% מעל הממוצע</span><br />
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$categoryInteractionPercentage }}% מתחת לממוצע</span><br />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Flow Chart -->
        <div class="card mb-1 col-4 flowchartpart" style="height:543px">
                    <div class="card-header d-flex justify-content-between align-items-end">
                        <h4 class="text-bold-800 color-dblack"> מוצרים מעודדי קניה  </h4>
                        <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="המטרה היא להגדיל את מדד האימופלס ולמדוד את המותג שלנו לעומת המדד בקטגוריה"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                    </div>
            <div class="card-content">
                      <div class="card-body pt-0 row d-flex align-items-center chartbox flowchart" style="position: relative;">
				@if(isset($beforedata[0]))
					<div id="no1" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[0]}}<br><span>{{$beforedata[1]}}%</span></div></div>
				@else
					<div id="no1" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
				@endif
				@if(isset($beforedata[2]))
					<div id="no2" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[2]}}<br><span>{{$beforedata[3]}}%</span></div></div>
				@else
					<div id="no2" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
				@endif
				@if(isset($beforedata[4]))
					<div id="no3" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[4]}}<br><span>{{$beforedata[5]}}%</span></div></div>
				@else
					<div id="no3" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
				@endif
              <div class="col-xs-4 col-md-4 col-4 centerarrow r1 row1">
                <div class="myarrow">
                   <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow r2 row1">
                <div class="myarrow">
                   <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow r3 row1">
                <div class="myarrow">
                  <!--<span class="arrow left"></span>-->
                  <span class="line"></span>
                  <!--<span class="arrow right"></span>-->
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow11 row1">
                <div class="myarrow">
                  <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow12 row1"></div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow13 row1">
                <div class="myarrow">
                   <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow r1 last row1">
                <div class="myarrow">
                   <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow12 last row1"></div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow r3 last row1 ">
                <div class="myarrow">
                  <!--<span class="arrow left"></span>-->
                  <span class="line"></span>
                  <!--<span class="arrow right"></span>-->
                </div>
              </div>
              <div class="col-xs-2 col-md-2 col-2"></div>
			 <div id="centerbox" class="col-xs-7 col-md-7 col-7">
				<div class="fchart"> {{ $product }} </div>
			</div>
			<div class="col-xs-3 col-md-3 col-3"></div>
					<div class="col-xs-4 col-md-4 col-4 centerarrow r1 last row2">
						<div class="myarrow">
							<span class="line"></span>
						</div>
					</div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow12 last row2"></div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow r3 last row2">
                <div class="myarrow">
                  <!--<span class="arrow left"></span>-->
                  <span class="line"></span>
                  <!--<span class="arrow right"></span>-->
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow11 row2">
                <div class="myarrow">
                  <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow12 row2"></div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow13 row2">
                <div class="myarrow">
                   <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow r1 row2">
                <div class="myarrow">
                   <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow r2 row2">
                <div class="myarrow">
                   <span class="line"></span>
                </div>
              </div>
              <div class="col-xs-4 col-md-4 col-4 centerarrow r3 row2">
                <div class="myarrow">
                  <!--<span class="arrow left"></span>-->
                  <span class="line"></span>
                  <!--<span class="arrow right"></span>-->
                </div>
              </div>
			@if(isset($afterdata[0]))
				<div id="no4" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[0]}}<br><span>{{$afterdata[1]}}%</span></div></div>
			@else
				<div id="no4" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
			@endif
			@if(isset($afterdata[2]))
				<div id="no5" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[2]}}<br><span>{{$afterdata[3]}}%</span></div></div>
			@else
				<div id="no5" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
			@endif
			@if(isset($afterdata[4]))
				<div id="no6" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[4]}}<br><span>{{$afterdata[5]}}%</span></div></div>
			@else
				<div id="no6" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
			@endif
            </div>
          </div>
        </div>
        <!-- Impulse Buying -->
          <div class="col-lg-3 col-12 match-height">
              <div class="card mb-1 col-12 carddata" >
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="text-bold-800 color-dblack"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="brain" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-brainx" style="width: 15px;"><path fill="currentColor" d="M208 0c-29.9 0-54.7 20.5-61.8 48.2-.8 0-1.4-.2-2.2-.2-35.3 0-64 28.7-64 64 0 4.8.6 9.5 1.7 14C52.5 138 32 166.6 32 200c0 12.6 3.2 24.3 8.3 34.9C16.3 248.7 0 274.3 0 304c0 33.3 20.4 61.9 49.4 73.9-.9 4.6-1.4 9.3-1.4 14.1 0 39.8 32.2 72 72 72 4.1 0 8.1-.5 12-1.2 9.6 28.5 36.2 49.2 68 49.2 39.8 0 72-32.2 72-72V64c0-35.3-28.7-64-64-64zm368 304c0-29.7-16.3-55.3-40.3-69.1 5.2-10.6 8.3-22.3 8.3-34.9 0-33.4-20.5-62-49.7-74 1-4.5 1.7-9.2 1.7-14 0-35.3-28.7-64-64-64-.8 0-1.5.2-2.2.2C422.7 20.5 397.9 0 368 0c-35.3 0-64 28.6-64 64v376c0 39.8 32.2 72 72 72 31.8 0 58.4-20.7 68-49.2 3.9.7 7.9 1.2 12 1.2 39.8 0 72-32.2 72-72 0-4.8-.5-9.5-1.4-14.1 29-12 49.4-40.6 49.4-73.9z" class=""></path></svg> מדד ה IMPULSE BUYING  </h4>
                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[13] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                </div>
                <hr class="m-10 mb-2" />
                <div class="card-content">
                  <div class="card-body pt-0">
                    <p class="color-dblack">שכלול הנתונים בין שניות מציאת המוצר לשניות של קבלת ההחלטה ולאחוז הנטישה</p>
                    <!-- <p class="color-dblack">מדד משוכלל למספר נתונים כגון: זמן מציאת המוצר, זמן התלבטות הרכישה, זגזוג בין מוצרים / מותגים שונים ועוד. <br />המטרה היא להגדיל מדד זה על מנת ליצור רכישה אימפולסיבית ככל הניתן ובעלת השפעה על הקונה בזמן הקניה. <br />ככל שהרכישה יותר אימפולסיבית לצרכן יש דחף בלתי רציונלי לרכוש מוצר כלשהו בנקודת הרכישה. <br />ככל שמדד האימפולס עולה כך יורד מדד הרכישה הרציונלית.</p> -->
                      <div class="height-150 barwrap">
                        <canvas id="bar-chart" data-values="{{ $productImpulse }},{{ $brandImpulse }},{{ $categoryImpulse }}"></canvas>
                      </div>
                      <div class="col-12">
						@if($productImpulse > $categoryImpulse)
							<i class="feather icon-arrow-up color-green"></i>
							<span class="font-medium-1 text-bold-600">מהממוצע בקטגוריה ‏{{ $productImpulse-$categoryImpulse }}% יותר אימפולס במוצר</span><br />
						@else
							<i class="feather icon-arrow-down color-orange"></i>
							<span class="font-medium-1 text-bold-600">מהממוצע בקטגוריה ‏{{ $categoryImpulse-$productImpulse }}% פָּחוּת אימפולס במוצר</span><br />
						@endif
                      </div>
                  </div>
                </div>
              </div>
            <div class="card mb-1 col-12 carddata" style="height:195px">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="text-bold-800 color-dblack">תובנות והמלצות</h4>
                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[15] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                </div>
                <hr class="m-10 mb-2" />
                <div class="card-content">
                    <div class="card-body pt-0">
					@foreach($recommendations as $index => $recommendation)
                        <div class="d-flex justify-content-between align-items-end">
                            <p class="text-bold-200 color-dblack">
							@if( $recommendation->icon == 'Red')
								<i class="feather icon-alert-circle cursor-pointer color-orange"></i>
							@else
								<i class="fa fa-thumbs-o-up cursor-pointer color-green"></i>
							@endif
							{{ $recommendation->recommendation }}</p>
                            <button type="button" data-toggle="modal"
              data-target="#recommendation{{ $index }}modal" class="btn btn-sm btn-outline-primary mr-1 mb-1 waves-effect waves-light font-medium-1 btn-dblack">{{ $recommendation->recommendationButton }}</button>
							<!-- Recommendation Modal {{ $index }} -->
							<div class="modal fade" id="recommendation{{ $index }}modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalCenterTitle">{{ $recommendation->recommendation }}</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											{!! $recommendation->notes !!}
										</div>
									</div>
								</div>
							</div>
							<!-- Recommendation Modal {{ $index }} -->
                    </div>
					@endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
  </section>

  @else
		<!-- error 404 -->
<section class="row">
  <div class="col-xl-4 col-md-4 col-12 offset-4 d-flex justify-content-center">
    <div class="card auth-card bg-transparent shadow-none rounded-0 mb-0 w-100">
      <div class="card-content">
        <div class="card-body text-center">
          <img src="{{ asset('images/pages/404.png') }}" class="img-fluid align-self-center" alt="branding logo">
          <h1 class="font-large-2 my-1">לא נמצאו מספיק נתונים!</h1>
          <p class="p-2">
			אין לנו מספיק נתונים כדי להציג תוצאות התואמות את הקריטריונים שלך. אנא לחץ על הלחצן למטה כדי לבחור קריטריונים אחרים.
          </p>
          <a class="btn btn-primary btn-lg mt-2" href="/search-information">חזרה לחיפוש</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- error 404 end -->
@endif
  {{-- Dashboard Ecommerce ends --}}
@endsection
@section('vendor-script')
{{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/charts/chart.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/charts/echarts/echarts.min.js')) }}"></script>
		<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/pages/apexChart.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/modal/components-modal.js')) }}"></script>
@endsection
