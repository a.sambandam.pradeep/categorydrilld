
@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
		<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
		@if($noresults != 0)
		<link rel="stylesheet" href="{{ asset(mix('css/pages/error.css')) }}">
		@endif
        <link rel="stylesheet" href="{{ asset(mix('css/pages/dashboard-ecommerce.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/card-analytics.css')) }}">
@endsection
@section('content')
  {{-- Dashboard Ecommerce Starts --}}
  @if($noresults == 0)  
    <section id="dashboard-ecommerce">
        <input type="hidden" name="jsonurl" id="jsonurl" value="{{asset('json/merge_dashboard.json') }}" />
        <input type="hidden" name="brandslug" id="brandslug" value="{{ $brandslug }}" />
        <input type="hidden" name="jsondata" id="jsondata" value="{{ $jsondata }}" />
        
        
        <!-- Tab 1 Starts -->
        <div id="tab1data" class="tabcontent active">
            <div class="row match-height firstrow j-center" style="direction: rtl; text-align: right;">            
            @if($inscount > 0)               
                @foreach($insights as $key => $insight)
                <div class="col-lg-4 col-sm-12 moreBox blogBox animate__animated animate__zoomIn"  @if($key > 8)style="display:none;"@endif>
                    <div class="card">
                        <div class="card-header">
                            <div class=" d-flex valign-middle">
                                <img src="{{asset('uploads/'.$insight->icon) }}" alt="" width="40px" />
                                <h3 class="card-title w-100 text-uppercase" style="margin-right: 20px;">
                                {{ $insight->title }}
                                </h3>
                            </div>
                            <div class="heading-elements">
                                <!--<ul class="list-inline mb-0">
                                    <li><span><i class="feather icon-more-vertical"></i></span></li>
                                </ul>-->
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">                                
                                <div class="justify-content-start mt-2">
                                    <h3>{!! $insight->maintext !!}</h3>
                                    <p>{{ $insight->datetext }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @if($inscount > 9)
                <div id="loadMore" style="" class="animate__animated animate__fadeIn">
                    <a href="#">Load More</a>
                </div>
                @endif
            @else
                <h2 class="noins"> No Insights found.</h2>
            @endif
            </div>
        </div>
        <!-- Tab 1 Ends -->

        <!-- Tab 2 Starts -->
        <div id="tab2data" class="tabcontent">
        
            <div class="row match-height firstrow j-center {{ ($buyerinsights > 3) ? 'owl-carousel' : '' }}" style="direction: rtl; text-align: right;">
        
                @foreach($buyerinsights as $insight)
                <div class="col-lg-4 col-sm-12 item">
                    <div class="card">
                        <div class="card-header">
                            <div class=" d-flex valign-middle">
                                <img src="{{asset('uploads/'.$insight->icon) }}" alt="" width="40px" />
                                <h3 class="card-title w-100 text-uppercase" style="margin-right: 20px;">
                                {{ $insight->title }}
                                </h3>
                            </div>
                            <div class="heading-elements">
                                <!--<ul class="list-inline mb-0">
                                    <li><span><i class="feather icon-more-vertical"></i></span></li>
                                </ul>-->
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">                                
                                <div class="justify-content-start mt-2">
                                    <h3>{!! $insight->maintext !!}</h3>
                                    <p>{{ $insight->datetext }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row d-flex">
                <div class="col-6 full125">
                    <div class="row d-flex justify-content-between align-items-end pad-bottom20">
                        <div class="col-md-3 col-6">
                            <h3 class="text-bold-700 color-dblack">Buyer Profile</h3>
                        </div>
                        <div class="col-md-9 col-6">
                            <hr />
                        </div>
                    </div>
                    <div class="row match-height firstrow">
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-end">
                                    <h4 class="text-bold-800 color-dblack">Average Age</h4>
                                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[0] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                                </div>
                                <hr class="m-10 mb-2" />
                                <div class="card-content">
                                    <div class="card-body pt-0 row d-flex align-items-center">
                                        <div id="chart1" class="col-md-6 col-12 doughcharts mb-1" data-labels="5 - 25,25 - 35,35 - 45,45 - 55,55 - 65,65+" data-colors="#5A30DC,#9013FE,#DAB0E8,#b561cf,#DA4CB2,#EB87CF" data-labelcolors="#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A"></div>
                                        <div class="col-md-3 col-6">
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color1"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">5 - 25</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color2"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">25 - 35</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color3"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">35 - 45</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-6">
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color4"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">45 - 55</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color5"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">55 - 65</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color6"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">65+</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="m-10" />
                                <div class="card-content">
                                    <div class="card-body pt-0 row d-flex align-items-center">
                                        <div class="series-result">
                                            <i class="feather icon-arrow-up color-green"></i>
                                            <span>Percentage change compared to all - <span id="age1diff"></span>%</span><br />&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-end">
                                    <h4 class="text-bold-800 color-dblack">Gender Segmentation</h4>
                                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[1] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                                </div>
                                <hr class="m-10 mb-2" />
                                <div class="card-content">
                                    <div class="card-body pt-0 row d-flex align-items-center">
                                        <div id="chart2" class="col-md-6 col-12 doughcharts mb-1" data-labels="Women, Men" data-icon="f007" data-colors="#19ABDE,#186BA1" data-labelcolors="#4A4A4A,#4A4A4A"></div>
                                        <div class="col-md-3 col-6">
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color7"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">Women</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color8"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">Men</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">

                                        </div>
                                    </div>
                                </div>
                                <hr class="m-10" />
                                <div class="card-content">
                                    <div class="card-body pt-0 row d-flex align-items-center">
                                        <div class="series-result">
                                                <i class="feather icon-arrow-up color-green"></i>
                                                <span>Male Percentage change compared to all - <span id="m1diff"></span>%</span><br />
                                                <i class="feather icon-arrow-up color-green"></i>
                                                <span>Female Percentage change compared to all - <span id="f1diff"></span>%</span><br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="row secondrow">                        
                       
                            <div class="col-lg-12 col-12 match-height">
                            <div class="card mb-1 col-12">
                                <div class="card-header d-flex justify-content-between align-items-end">
                                    <h4 class="text-bold-800 color-dblack">Demographic segmentation</h4>
                                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[2] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>                    
                                </div>
                                <hr class="m-10 mb-2" />
                                <div class="card-content">
                                    <div class="card-body pt-0">
                                        <div class="barwrap">
                                            <canvas id="groupChart1"></canvas>
                                        </div>					
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-6 full125">
                    <div class="row d-flex justify-content-between align-items-end pad-bottom20">
                        <div class="col-md-3 col-6">
                            <h3 class="text-bold-700 color-dblack">Potential buyer</h3>
                        </div>
                        <div class="col-md-9 col-6">
                            <hr />
                        </div>
                    </div>
                    <div class="row match-height firstrow">
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-end">
                                    <h4 class="text-bold-800 color-dblack">Average Age</h4>
                                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[3] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                                </div>
                                <hr class="m-10 mb-2" />
                                <div class="card-content">
                                    <div class="card-body pt-0 row d-flex align-items-center">
                                        <div id="chart3" class="col-md-6 col-12 doughcharts mb-1" data-labels="5 - 25,25 - 35,35 - 45,45 - 55,55 - 65,65+" data-colors="#5A30DC,#9013FE,#DAB0E8,#b561cf,#DA4CB2,#EB87CF" data-labelcolors="#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A"></div>
                                        <div class="col-md-3 col-6">
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color1"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">5 - 25</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color2"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">25 - 35</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color3"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">35 - 45</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-6">
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color4"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">45 - 55</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color5"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">55 - 65</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color6"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">65+</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="m-10" />
                                <div class="card-content">
                                    <div class="card-body pt-0 row d-flex align-items-center">
                                        <div class="series-result">
                                            <i class="feather icon-arrow-up color-green"></i>
                                            <span>Percentage change compared to all - <span id="age2diff"></span>%</span><br />&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-end">
                                    <h4 class="text-bold-800 color-dblack">Gender Segmentation</h4>
                                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[4] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                                </div>
                                <hr class="m-10 mb-2" />
                                <div class="card-content">
                                    <div class="card-body pt-0 row d-flex align-items-center">
                                        <div id="chart4" class="col-md-6 col-12 doughcharts mb-1" data-labels="Women, Men" data-icon="f007" data-colors="#19ABDE,#186BA1" data-labelcolors="#4A4A4A,#4A4A4A"></div>
                                        <div class="col-md-3 col-6">
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color7"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">Women</span>
                                                </div>
                                            </div>
                                            <div class="chart-info d-flex justify-content-between mb-1">
                                                <div class="series-info d-flex align-items-center">
                                                    <i class="fa fa-circle font-medium-2 text-color8"></i>
                                                    <span class="text-bold-600 mx-50 color-lblack">Men</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">

                                        </div>
                                    </div>
                                </div>
                                <hr class="m-10" />
                                <div class="card-content">
                                    <div class="card-body pt-0 row d-flex align-items-center">
                                        <div class="series-result">
                                            <i class="feather icon-arrow-up color-green"></i>
                                            <span>Male Percentage change compared to all - <span id="m2diff"></span>%</span><br />
                                            <i class="feather icon-arrow-up color-green"></i>
                                            <span>Female Percentage change compared to all - <span id="f2diff"></span>%</span><br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="row secondrow">                        
                        
                            <div class="col-lg-12 col-12 match-height">
                            <div class="card mb-1 col-12">
                                <div class="card-header d-flex justify-content-between align-items-end">
                                    <h4 class="text-bold-800 color-dblack">Demographic segmentation</h4>
                                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[5] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>                    
                                </div>
                                <hr class="m-10 mb-2" />
                                <div class="card-content">
                                    <div class="card-body pt-0">
                                        <div class="barwrap">
                                        <canvas id="groupChart2"></canvas>
                                        </div>					
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Tab 2 Ends -->        

        <!-- Tab 3 Starts -->
        <div id="tab3data" class="tabcontent">
            <div class="row match-height firstrow j-center {{ ($funnelinsights > 3) ? 'owl-carousel' : '' }}" style="direction: rtl; text-align: right;">
                @foreach($funnelinsights as $insight)
                <div class="col-lg-4 col-sm-12 item">
                    <div class="card">
                        <div class="card-header">
                            <div class=" d-flex valign-middle">
                                <img src="{{asset('uploads/'.$insight->icon) }}" alt="" width="40px" />
                                <h3 class="card-title w-100 text-uppercase" style="margin-right: 20px;">
                                {{ $insight->title }}
                                </h3>
                            </div>
                            <div class="heading-elements">
                                <!--<ul class="list-inline mb-0">
                                    <li><span><i class="feather icon-more-vertical"></i></span></li>
                                </ul>-->
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">                                
                                <div class="justify-content-start mt-2">
                                    <h3>{!! $insight->maintext !!}</h3>
                                    <p>{{ $insight->datetext }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row secondrow mh-80 j-center">
                    <div class="col-lg-6 col-12">
                        <div class="card mb-1 col-12">
                            <div class="card-header d-flex justify-content-between align-items-end">
                                <h4 class="text-bold-800 color-dblack">In-store audience involvement</h4>
                                <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[6] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                            </div>
                            <hr class="m-10 mb-2" />
                            <div class="card-content">
                                <div class="card-body pt-0">
                                <style>
                                #chartdiv {
                                width: 100%;
                                height: 300px;
                                }

                                </style>

                                <!-- Resources -->
                                <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
                                <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
                                <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
                                <!-- HTML -->
                                <div id="chartdiv"></div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between align-items-end twcolrow">
                            @if($onlycategory == 'yes')
                            <div class="card col-12 col-md-6 modified">
                                <div class="card-header d-flex justify-content-between align-items-center pad-rl0">
                                    <h4 class="text-bold-800 color-dblack"><span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[7] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span> Product abandonment</h4>
                                    
                                </div>
                                <hr class="m-10 mb-2" />
                                <div class="card-content">
                                    <div class="card-body pt-0">
                                        <div class="features row">
                                            <div class="col-12">
                                                <div class="progress bg-orange-pbar progress-xl">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width:50%">55%</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($onlycategory == 'yes')
                            <div class="card col-12 col-md-6 modified">
                            @endif
                            @if($onlycategory != 'yes')
                            <div class="card col-12 col-md-12 modified">
                            @endif
                            
                                <div class="card-header d-flex justify-content-between align-items-center pad-rl0">
                                    <h4 class="text-bold-800 color-dblack"><span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[8] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>  Category conversion ratio</h4>
                                    
                                </div>
                                <hr class="m-10 mb-2" />
                                <div class="card-content">
                                    <div class="card-body pt-0">
                                        <div class="features row">
                                            <div class="col-12">
                                                <div class="progress bg-blue-pbar progress-xl">
                                                    <div class="progress-bar ccratio" role="progressbar" aria-valuenow="65" aria-valuemin="65" aria-valuemax="100" style="width:65%">65%</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <!-- Flow Chart -->
                    <div class="card col-lg-6 flowchartpart" style="height:560px">
                                <div class="card-header d-flex justify-content-between align-items-end">
                                    <h4 class="text-bold-800 color-dblack"> Products that encourage impulse buying </h4>
                                    <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[9] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                                </div>
                        <div class="card-content">
                                <div class="card-body pt-0 row d-flex align-items-center chartbox flowchart" style="position: relative;">
                                @if($onlycategory == 'yes')
                                <div class="hideflow">Applicable only when a brand is chosen.</div>
                                @endif
                            @if(isset($beforedata[0]))
                                <div id="no1" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[0]}}<br><span>{{$beforedata[1]}}%</span></div></div>
                            @else
                                <div id="no1" class="col-xs-4 col-md-4 col-4"><div class="fchart">Not available<br><span>&nbsp;</span></div></div>
                            @endif
                            @if(isset($beforedata[2]))
                                <div id="no2" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[2]}}<br><span>{{$beforedata[3]}}%</span></div></div>
                            @else
                                <div id="no2" class="col-xs-4 col-md-4 col-4"><div class="fchart">Not available<br><span>&nbsp;</span></div></div>
                            @endif
                            @if(isset($beforedata[4]))
                                <div id="no3" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[4]}}<br><span>{{$beforedata[5]}}%</span></div></div>
                            @else
                                <div id="no3" class="col-xs-4 col-md-4 col-4"><div class="fchart">Not available<br><span>&nbsp;</span></div></div>
                            @endif
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r1 row1">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r2 row1">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r3 row1">
                            <div class="myarrow">
                            <!--<span class="arrow left"></span>-->
                            <span class="line"></span>
                            <!--<span class="arrow right"></span>-->
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow11 row1">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow12 row1"></div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow13 row1">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r1 last row1">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow12 last row1"></div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r3 last row1 ">
                            <div class="myarrow">
                            <!--<span class="arrow left"></span>-->
                            <span class="line"></span>
                            <!--<span class="arrow right"></span>-->
                            </div>
                        </div>
                        <div class="col-xs-2 col-md-2 col-2"></div>
                        <div id="centerbox" class="col-xs-7 col-md-7 col-7">
                            <div class="fchart"> {{ $brand }} </div>
                        </div>
                        <div class="col-xs-3 col-md-3 col-3"></div>
                                <div class="col-xs-4 col-md-4 col-4 centerarrow r1 last row2">
                                    <div class="myarrow">
                                        <span class="line"></span>
                                    </div>
                                </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow12 last row2"></div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r3 last row2">
                            <div class="myarrow">
                            <!--<span class="arrow left"></span>-->
                            <span class="line"></span>
                            <!--<span class="arrow right"></span>-->
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow11 row2">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow12 row2"></div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow13 row2">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r1 row2">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r2 row2">
                            <div class="myarrow">
                            <span class="line"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-4 centerarrow r3 row2">
                            <div class="myarrow">
                            <!--<span class="arrow left"></span>-->
                            <span class="line"></span>
                            <!--<span class="arrow right"></span>-->
                            </div>
                        </div>
                        @if(isset($afterdata[0]))
                            <div id="no4" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[0]}}<br><span>{{$afterdata[1]}}%</span></div></div>
                        @else
                            <div id="no4" class="col-xs-4 col-md-4 col-4"><div class="fchart">Not available<br><span>&nbsp;</span></div></div>
                        @endif
                        @if(isset($afterdata[2]))
                            <div id="no5" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[2]}}<br><span>{{$afterdata[3]}}%</span></div></div>
                        @else
                            <div id="no5" class="col-xs-4 col-md-4 col-4"><div class="fchart">Not available<br><span>&nbsp;</span></div></div>
                        @endif
                        @if(isset($afterdata[4]))
                            <div id="no6" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[4]}}<br><span>{{$afterdata[5]}}%</span></div></div>
                        @else
                            <div id="no6" class="col-xs-4 col-md-4 col-4"><div class="fchart">Not available<br><span>&nbsp;</span></div></div>
                        @endif
                        </div>
                    </div>
                    </div>
                </div>

        </div>
        <!-- Tab 3 Ends -->

        <!-- Tab 4 Starts -->
        <div id="tab4data" class="tabcontent">
            <div class="row match-height firstrow j-center {{ ($saleinsights > 3) ? 'owl-carousel' : '' }}" style="direction: rtl; text-align: right;">
                @foreach($saleinsights as $insight)
                <div class="col-lg-4 col-sm-12 item">
                    <div class="card">
                        <div class="card-header">
                            <div class=" d-flex valign-middle">
                                <img src="{{asset('uploads/'.$insight->icon) }}" alt="" width="40px" />
                                <h3 class="card-title w-100 text-uppercase" style="margin-right: 20px;">
                                {{ $insight->title }}
                                </h3>
                            </div>
                            <div class="heading-elements">
                                <!--<ul class="list-inline mb-0">
                                    <li><span><i class="feather icon-more-vertical"></i></span></li>
                                </ul>-->
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">                                
                                <div class="justify-content-start mt-2">
                                    <h3>{!! $insight->maintext !!}</h3>
                                    <p>{{ $insight->datetext }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row match-height firstrow mh-80">
                <div class="col-12">
                <div class="accordion" id="accordionExample" data-toggle-hover="true" style="direction: rtl; text-align: right;">
                    <div class="collapse-margin">
                        <div class="card-header" id="headingOne" data-toggle="collapse" role="button"
                            data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        <span class="lead collapse-title collapsed" style="color:#000;">
                            Cross merchandising - What additional items are being purchased with Tuna ?
                        </span>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body row">
                            <div class="col-8 col-offset-2"><canvas id="bar-chart" dir="rtl"></canvas></div>
                        </div>
                        </div>
                    </div>
                    <div class="collapse-margin">
                        <div class="card-header" id="headingTwo" data-toggle="collapse" role="button"
                            data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <span class="lead collapse-title collapsed" style="color:#000;">
                            Tuna category     
                        </span>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body row" style="padding: 30px;">
                            <div class="col-4 col-offset-4 graphholder">
                                <h2><i class="feather icon-cloud-snow"></i>  The effect of weather - Tuna category</h2>
                                <i class="feather icon-cloud-rain"></i>
                                <i class="feather icon-sun"></i>
                                <canvas id="bar-chart2" dir="rtl"></canvas>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="collapse-margin">
                        <div class="card-header" id="headingThree" data-toggle="collapse" role="button" data-target="#collapseThree"
                            aria-expanded="false" aria-controls="collapseThree">
                        <span class="lead collapse-title" style="color:#000;">
                            Heat/Correlation/Percentage vs. segment
                        </span>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body row" style="padding: 30px;">
                            <div class="col-6 graphholder">
                                <h2><i class="feather icon-cloud-snow"></i> By brand</h2>
                                <i class="feather icon-cloud-rain"></i>
                                <i class="feather icon-sun"></i>
                                <canvas id="bar-chart3" dir="rtl"></canvas>
                            </div>
                            <div class="col-6 graphholder">
                                <h2><i class="feather icon-cloud-snow"></i> Temperature correlation </h2>
                                <i class="feather icon-cloud-rain"></i>
                                <i class="feather icon-sun"></i>
                                <canvas id="bar-chart4" dir="rtl"></canvas>
                            </div>
                        </div>
                        </div>
                    </div>                    
                    </div>
                </div>
            </div>
        </div>
        <!-- Tab 4 Ends -->

        <!-- Tab 5 Starts -->
        <div id="tab5data" class="tabcontent">
            <div class="row match-height firstrow j-center mh-80">
                    <div class="col-4">
                        <img src="{{asset('images/img1.jpeg') }}" alt="Shelf Image" height="auto" width="100%" id="bigimg" /> 
                    </div>
                    <div class="col-8">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body pt-0 row d-flex align-items-center">
                                    <br /><br /><br /><h2>Planogram data table</h2>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered mb-0">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Planogram image</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Product price</th>
                                                    <th scope="col">The impulse index</th>
                                                    <th scope="col">Purchase percentages</th>
                                                    <th scope="col">Sales</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="#" class="openbigimg" data-src="{{asset('images/img1.jpeg') }}"><img src="{{asset('images/img1.jpeg') }}" alt="Shelf behavior" height="auto" width="64px" /> <i class="fa fa-external-link"></i> </a></td>
                                                    <td>12/8/2021-11/9/2021</td>
                                                    <td>8.9 NIS</td>
                                                    <td>40% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-95% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-12% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                </tr>   
                                                <tr>
                                                    <td><a href="#" class="openbigimg" data-src="{{asset('images/img2.jpeg') }}"><img src="{{asset('images/img2.jpeg') }}" alt="Shelf behavior" height="auto" width="64px" /> <i class="fa fa-external-link"></i> </a></td>
                                                    <td>10/6/2021-17/7/2021</td>
                                                    <td>8.9 NIS</td>
                                                    <td>55% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>+10% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-2.5% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                </tr> 
                                                <tr>
                                                    <td><a href="#" class="openbigimg" data-src="{{asset('images/img3.jpeg') }}"><img src="{{asset('images/img3.jpeg') }}" alt="Shelf behavior" height="auto" width="64px" /> <i class="fa fa-external-link"></i> </a></td>
                                                    <td>10/6/2021-17/7/2021</td>
                                                    <td>7.5 NIS</td>
                                                    <td>70% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-2% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>+25% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                </tr>   
                                                <tr>
                                                    <td><a href="#" class="openbigimg" data-src="{{asset('images/img4.jpeg') }}"><img src="{{asset('images/img4.jpeg') }}" alt="Shelf behavior" height="auto" width="64px" /> <i class="fa fa-external-link"></i> </a></td>
                                                    <td>10/6/2021-17/7/2021</td>
                                                    <td>8.9 NIS</td>
                                                    <td>55% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>+10% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-2.5% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                </tr>                                               
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>  
            </div>
        </div>
        <!-- Tab 5 Ends -->

        <!-- Tab 6 Starts -->
        <div id="tab6data" class="tabcontent">
        </div>
        <!-- Tab 6 Ends -->

        <!-- Tab 7 Starts -->
        <div id="tab7data" class="tabcontent">
            <video width="100%" height="100%" controls>
                <source src="{{asset('images/DRILL_Women_35_Emotion.mp4') }}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
        <!-- Tab 7 Ends -->
    </section>

  @else
		<!-- error 404 -->
<section class="row">
  <div class="col-xl-4 col-md-4 col-12 offset-4 d-flex justify-content-center">
    <div class="card auth-card bg-transparent shadow-none rounded-0 mb-0 w-100">
      <div class="card-content">
        <div class="card-body text-center">
          <img src="{{ asset('images/pages/404.png') }}" class="img-fluid align-self-center" alt="branding logo">
          <h1 class="font-large-2 my-1">לא נמצאו מספיק נתונים!</h1>
          <p class="p-2">
			אין לנו מספיק נתונים כדי להציג תוצאות התואמות את הקריטריונים שלך. אנא לחץ על הלחצן למטה כדי לבחור קריטריונים אחרים.
          </p>
          <a class="btn btn-primary btn-lg mt-2" href="/search-information">חזרה לחיפוש</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- error 404 end -->
@endif
  {{-- Dashboard Ecommerce ends --}}
@endsection
@section('vendor-script')
{{-- vendor files --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
        <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>        
        <script src="{{ asset(mix('vendors/js/charts/echarts/echarts.min.js')) }}"></script>
		<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css" integrity="sha512-C8Movfk6DU/H5PzarG0+Dv9MA9IZzvmQpO/3cIlGIflmtY3vIud07myMu4M/NTPJl8jmZtt/4mC9bAioMZBBdA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/pages/apexChart.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/modal/components-modal.js')) }}"></script>
@endsection