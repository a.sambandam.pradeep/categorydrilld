
@extends('layouts/fullLayoutMaster')

@section('title', 'Data Form')
@section('vendor-style')
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
@endsection
@section('content')
@if($user->name == 'Eyal')
<script>window.location = "/search-information";</script>
@endif
<style>
    .app-content.content{
        background: transparent linear-gradient(180deg, #8E2DE2 0%, #4A00E0 100%) 0% 0% no-repeat padding-box;
		min-height: 100vh;
    }
	
    </style>
<!-- Basic Vertical form layout section start -->
<section id="basic-vertical-layouts">
  <div class="row match-height">
      
      <div class="col-md-8 offset-md-2 col-12  align-items-center">
          <div class="card">
              <div class="card-content">
                  <div class="card-body">
                    <h4 class="text-bold-800 color-dblack font-medium-4">{{ $greetings }}, {{ $user->name }}</h4><a href="auth-logout"><i class="feather icon-power"></i> logout</a>
                    <hr />      
                    <div class="d-flex justify-content-left align-items-end">
						<a class="btn btn-lg btn-warning mr-1 mb-1 waves-effect waves-light" href="/search-information">Searching for information</a>
						@if($user->name != 'Eyal')
						<a class="btn btn-lg btn-warning mr-1 mb-1 waves-effect waves-light" href="/upload-data"> Upload JSON</a>
						<!--<button type="button" data-toggle="modal" data-backdrop="static" 
              data-target="#confirmationmodal" class="btn btn-lg btn-warning mr-1 mb-1 waves-effect waves-light">Deleting information from a database</button>-->
						<!-- Confirmation Modal -->
						<div class="modal fade" id="confirmationmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable modal-sm" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalCenterTitle">מחיקת מידע ממסד נתונים</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										האם אתה בטוח שברצונך לנקות? שימו לב שזה בלתי הפיך.<br /><br />
										<div class="align-items-center d-flex">
											<a class="btn btn-lg btn-warning mr-1 mb-1 waves-effect waves-light" href="/cleanup-db">
												כן
											</a>
											<button type="button" class="btn btn-lg btn-warning mr-1 mb-1 waves-effect waves-light" data-dismiss="modal" aria-label="Close">
												לא
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Confirmation Modal -->
						<a class="btn btn-lg btn-warning mr-1 mb-1 waves-effect waves-light" href="/update-text">Upload of recommendations.</a>
						@endif
					</div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
<!-- // Basic Vertical form layout section end -->

@endsection

@section('vendor-script')
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
@endsection
@section('page-script')
        <!-- Page js files -->
        <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>
@endsection
