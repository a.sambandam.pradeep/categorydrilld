@if($configData["mainLayoutType"] == 'horizontal')
  <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu {{ $configData['navbarColor'] }} navbar-fixed">
      <div class="navbar-header d-xl-block d-none">
          <ul class="nav navbar-nav flex-row">
              <li class="nav-item"><a class="navbar-brand" href="dashboard-analytics">
                  <div class="brand-logo"></div></a></li>
          </ul>
      </div>
  @else
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu {{ $configData['navbarClass'] }} navbar-light navbar-shadow {{ $configData['navbarColor'] }}">
  @endif
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse d-flex" id="navbar-mobile">                
                 <ul class="nav navbar-nav float-left col-6">                    
                    <li class="nav-item mobile-menu mhandle" style="display:none;"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>                    
                    <li class="dropdown dropdown-user nav-item d-flex align-items-center pad-right40">
					   <span><a href="/"><img src="{{asset('images/logo/drilldlogo.jpg') }}" alt="Drill-D Logo" height="45" width="45" /></a></span>
                    </li>
                    <li class="dropdown dropdown-notification nav-item d-flex align-items-center">
						<h2>&nbsp;&nbsp;&nbsp;<?php echo date('h:i   |   d/m/Y'); ?></h2>
                    </li>
                </ul>
                <div class="float-right bookmark-wrapper align-items-center text-right col-6">
                    <div class="float-right d-flex" style="direction: rtl; align-items:center;">
                        <h1 class="color-dblack text-bold-600" style="margin-left: 30px;">@isset($category) {{$category}} @endisset @isset($onlycategory) @if($onlycategory != 'yes') > {{$brand}} @endif @endisset</h1>                        
                        <ul class="float-right nav navbar-nav">
                            <li class="dropdown dropdown-notification nav-item">
                                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true"><i class="ficon feather icon-search"></i></a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="scrollable-container media-list ps">
                                        <a class="d-flex justify-content-between" href="javascript:void(0)">
                                            <div class="media d-flex align-items-start">
                                                @include('panels/searchform');
                                            </div>
                                        </a>
                                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps__rail-y" style="top: 0px; right: 0px;">
                                            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                        </div>
                                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

{{-- Search Start Here --}}
<ul class="main-search-list-defaultlist d-none">
    <li class="d-flex align-items-center">
        <a class="pb-25" href="#">
            <h6 class="text-primary mb-0">Files</h6>
        </a>
    </li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a class="d-flex align-items-center justify-content-between w-100" href="#">
            <div class="d-flex">
                <div class="ml-0 mr-50"><img src="{{ asset('images/icons/xls.png') }}" alt="png" height="32" /></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small></div>
            </div><small class="search-data-size mr-50 text-muted">&apos;17kb</small></a>
    </li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a class="d-flex align-items-center justify-content-between w-100" href="#">
            <div class="d-flex">
                <div class="ml-0 mr-50"><img src="{{ asset('images/icons/jpg.png') }}" alt="png" height="32" /></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small></div>
            </div><small class="search-data-size mr-50 text-muted">&apos;11kb</small></a>
    </li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a class="d-flex align-items-center justify-content-between w-100" href="#">
            <div class="d-flex">
                <div class="ml-0 mr-50"><img src="{{ asset('images/icons/pdf.png') }}" alt="png" height="32" /></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small></div>
            </div><small class="search-data-size mr-50 text-muted">&apos;150kb</small></a>
    </li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a class="d-flex align-items-center justify-content-between w-100" href="#">
            <div class="d-flex">
                <div class="ml-0 mr-50"><img src="{{ asset('images/icons/doc.png') }}" alt="png" height="32" /></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small></div>
            </div><small class="search-data-size mr-50 text-muted">&apos;256kb</small></a>
    </li>
    <li class="d-flex align-items-center">
        <a class="pb-25" href="#">
            <h6 class="text-primary mb-0">Members</h6>
        </a>
    </li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
            <div class="d-flex align-items-center">
                <div class="avatar mr-50"><img src="{{ asset('images/portrait/small/avatar-s-8.jpg') }}" alt="png" height="32" /></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small></div>
            </div>
        </a>
    </li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
            <div class="d-flex align-items-center">
                <div class="avatar mr-50"><img src="{{ asset('images/portrait/small/avatar-s-1.jpg') }}" alt="png" height="32" /></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small></div>
            </div>
        </a>
    </li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
            <div class="d-flex align-items-center">
                <div class="avatar mr-50"><img src="{{ asset('images/portrait/small/avatar-s-14.jpg') }}" alt="png" height="32" /></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small></div>
            </div>
        </a>
    </li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer">
        <a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
            <div class="d-flex align-items-center">
                <div class="avatar mr-50"><img src="{{ asset('images/portrait/small/avatar-s-6.jpg') }}" alt="png" height="32" /></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small></div>
            </div>
        </a>
    </li>
</ul>
<ul class="main-search-list-defaultlist-other-list d-none">
    <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">
        <a class="d-flex align-items-center justify-content-between w-100 py-50">
            <div class="d-flex justify-content-start"><span class="mr-75 feather icon-alert-circle"></span><span>No results found.</span></div>
        </a>
    </li>
</ul>
{{-- Search Ends --}}
<!-- END: Header-->
