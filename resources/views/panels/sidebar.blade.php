@php
    $configData = Helper::applClasses();
@endphp
<div class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="dashboard-analytics">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">DrillD</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            
            <li class="active">
            <a href="#" class="d-flex tabref" data-tabid="1">
                    <img src="{{asset('images/icons/6.png') }}" alt="Decision tree" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Insights</strong></span>
                </a>
            </li>
            <li>
            <a href="#" class="d-flex tabref" data-tabid="2">
                    <img src="{{asset('images/icons/2.png') }}" alt="Buyers" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Buyers</strong><br /><span>Demographic+ buyer personality</span></span>
                </a>
            </li>
            <li>
            <a href="#" class="d-flex tabref" data-tabid="3">
                    <img src="{{asset('images/icons/3.png') }}" alt="Conversion funnel" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Conversion funnel</strong><br /><span>rush hours/conversion rate/undecided/abandonment</span></span>
                </a>
            </li>
            <li>
            <a href="#" class="d-flex tabref" data-tabid="4">
                    <img src="{{asset('images/icons/4.png') }}" alt="Pricing and promotion analysis" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Sales</strong></span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
