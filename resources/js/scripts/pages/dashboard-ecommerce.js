/*=========================================================================================
    File Name: dashboard-ecommerce.js
    Description: dashboard ecommerce page content with Apexchart Examples
    ----------------------------------------------------------------------------------------
    Item name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

//const { round } = require("lodash");
$( document ).ready(function () {
	$(".moreBox").slice(0, 3).show();
	if ($(".blogBox:hidden").length != 0) {
			$("#loadMore").show();
	}               
	$("#loadMore").on('click', function (e) {
		e.preventDefault();
		$(".moreBox:hidden").slice(0, 3).slideDown();
		if ($(".moreBox:hidden").length == 0) {
				$("#loadMore").fadeOut('slow');
		}
	});
});
$(window).on("load", function () {
	$('.mhandle').show();
	var $primary = '#7367F0';
	var $success = '#28C76F';
	var $danger = '#EA5455';
	var $warning = '#FF9F43';
	var $info = '#00cfe8';
	var $primary_light = '#A9A2F6';
	var $danger_light = '#f29292';
	var $success_light = '#55DD92';
	var $warning_light = '#ffc085';
	var $info_light = '#1fcadb';
	var $strok_color = '#b9c3cd';
	var $label_color = '#e7e7e7';
	var $white = '#fff';	
	var jsonurl = $('#jsonurl').val();	
	var brandslug = $('#brandslug').val();
	var jsondata,myBarChart1,myBarChart2;
	
	$.getJSON( jsonurl, function( data ) {
		updateCharts(data,'yes');
		jsondata = data;
	});
	function filterJson(jsondata,action){
		return jsondata.annotations.filter(function(n){
			var buyerDetails = jsondata.buyers.filter(buyer => buyer.id === n.buyer_id)
			if(n.attributes.action == action){
				if((action == 'pu' || action == 'pb') && brandslug != 'all'){
					if(n.attributes.location_on_shelf != undefined){
						if(n.attributes.location_on_shelf.name == brandslug){
							n.buyer_age = buyerDetails[0].age
							n.buyer_gender = buyerDetails[0].gender
							return n
						}
					}
				}else{
					n.buyer_age = buyerDetails[0].age
					n.buyer_gender = buyerDetails[0].gender
					return n
				}
				
			}
		});
	}

	function filterJsonDemographic(jsondata,from,to,gender){
		return jsondata.filter(function(n){
			if(from != '' && to != '' && gender != ''){
				if((parseInt(n.buyer_age) >= parseInt(from)) && (parseInt(n.buyer_age) <= parseInt(to)) && (n.buyer_gender == gender)){				
					return n
				}
			}
			if(from == '' && to == '' && gender != ''){
				if(n.buyer_gender == gender){				
					return n
				}
			}
			if(from != '' && to != '' && gender == ''){
				if((parseInt(n.buyer_age) >= parseInt(from)) && (parseInt(n.buyer_age) <= parseInt(to))){				
					return n
				}
			}		
			
		});
	}
	function updateCharts(jsondata, newdata){
		var allbuyers = jsondata.buyers;
		var totalbuyers = allbuyers.length;
		var totalage = 0;
		allbuyers.forEach(function callbackFn(element) { 
			totalage += parseInt(element.age);
		});
		var allavg = totalage/totalbuyers;

		var pubuyers =  filterJson(jsondata,'pu');
		var pbbuyers =  filterJson(jsondata,'pb');

		var totalpubuyers = pubuyers.length;
		var totalpuage = 0;
		var totalpuavg = 0;
		pubuyers.forEach(function callbackFn(element) { 			
			if(element.buyer_age != 0 && element.buyer_age != ''){
				totalpuage += parseInt(element.buyer_age);
				totalpuavg++;
			}
		});
		var puavg = totalpuage/totalpuavg;


		var totalpbbuyers = pbbuyers.length;
		var totalpbage = 0;
		var totalpbavg = 0;
		pbbuyers.forEach(function callbackFn(element) { 
			if(element.buyer_age != 0 && element.buyer_age != ''){
				totalpbage += parseInt(element.buyer_age);
				totalpbavg++;
			}
		});
		var pbavg = totalpbage/totalpbavg;
		
		var age1diff = (puavg/allavg)*100;
		$('#age1diff').html(Math.round(age1diff));

		var age2diff = (pbavg/allavg)*100;
		$('#age2diff').html(Math.round(age2diff));

		var puage1 = filterJsonDemographic(pubuyers,'5','24','');
		var puage2 = filterJsonDemographic(pubuyers,'25','34','');
		var puage3 = filterJsonDemographic(pubuyers,'35','44','');
		var puage4 = filterJsonDemographic(pubuyers,'45','54','');
		var puage5 = filterJsonDemographic(pubuyers,'55','64','');
		var puage6 = filterJsonDemographic(pubuyers,'65','100','');
		chart1data = puage1.length+','+puage2.length+','+puage3.length+','+puage4.length+','+puage5.length+','+puage6.length;

		var pum = filterJsonDemographic(pubuyers,'','','f');
		var puf = filterJsonDemographic(pubuyers,'','','m');
		chart2data = pum.length+','+puf.length;
		
		var pbage1 = filterJsonDemographic(pbbuyers,'5','24','');
		var pbage2 = filterJsonDemographic(pbbuyers,'25','34','');
		var pbage3 = filterJsonDemographic(pbbuyers,'35','44','');
		var pbage4 = filterJsonDemographic(pbbuyers,'45','54','');
		var pbage5 = filterJsonDemographic(pbbuyers,'55','64','');
		var pbage6 = filterJsonDemographic(pbbuyers,'65','100','');
		chart3data = pbage1.length+','+pbage2.length+','+pbage3.length+','+pbage4.length+','+pbage5.length+','+pbage6.length;

		var pbm = filterJsonDemographic(pbbuyers,'','','f');
		var pbf = filterJsonDemographic(pbbuyers,'','','m');
		chart4data = pbm.length+','+pbf.length;

		var puage1m = filterJsonDemographic(puage1,'','','m');
		var puage2m = filterJsonDemographic(puage2,'','','m');
		var puage3m = filterJsonDemographic(puage3,'','','m');
		var puage4m = filterJsonDemographic(puage4,'','','m');
		var puage5m = filterJsonDemographic(puage5,'','','m');
		var puage6m = filterJsonDemographic(puage6,'','','m');

		chart5datam = puage1m.length+','+puage2m.length+','+puage3m.length+','+puage4m.length+','+puage5m.length+','+puage6m.length;

		var puage1f = filterJsonDemographic(puage1,'','','f');
		var puage2f = filterJsonDemographic(puage2,'','','f');
		var puage3f = filterJsonDemographic(puage3,'','','f');
		var puage4f = filterJsonDemographic(puage4,'','','f');
		var puage5f = filterJsonDemographic(puage5,'','','f');
		var puage6f = filterJsonDemographic(puage6,'','','f');

		chart5dataf = puage1f.length+','+puage2f.length+','+puage3f.length+','+puage4f.length+','+puage5f.length+','+puage6f.length;

		var pbage1m = filterJsonDemographic(pbage1,'','','m');
		var pbage2m = filterJsonDemographic(pbage2,'','','m');
		var pbage3m = filterJsonDemographic(pbage3,'','','m');
		var pbage4m = filterJsonDemographic(pbage4,'','','m');
		var pbage5m = filterJsonDemographic(pbage5,'','','m');
		var pbage6m = filterJsonDemographic(pbage6,'','','m');

		chart6datam = pbage1m.length+','+pbage2m.length+','+pbage3m.length+','+pbage4m.length+','+pbage5m.length+','+pbage6m.length;

		var pbage1f = filterJsonDemographic(pbage1,'','','f');
		var pbage2f = filterJsonDemographic(pbage2,'','','f');
		var pbage3f = filterJsonDemographic(pbage3,'','','f');
		var pbage4f = filterJsonDemographic(pbage4,'','','f');
		var pbage5f = filterJsonDemographic(pbage5,'','','f');
		var pbage6f = filterJsonDemographic(pbage6,'','','f');

		chart6dataf = pbage1f.length+','+pbage2f.length+','+pbage3f.length+','+pbage4f.length+','+pbage5f.length+','+pbage6f.length;
		
		var totalpum = pum.length;
		var totalpuf = puf.length;
		
		var m1diff = (totalpum/totalbuyers)*100;
		$('#m1diff').html(Math.round(m1diff));

		var f1diff = (totalpuf/totalbuyers)*100;
		$('#f1diff').html(Math.round(f1diff));

		var totalpbm = pbm.length;
		var totalpbf = pbf.length;
		
		var m2diff = (totalpbm/totalbuyers)*100;
		$('#m2diff').html(Math.round(m2diff));

		var f2diff = (totalpbf/totalbuyers)*100;
		$('#f2diff').html(Math.round(f2diff));

	// Session Chart
	// ----------------------------------
		$('.doughcharts').each(function(){
			var divid = $(this).attr('id');
			var totalval;
			$(this).html('');
			var percentages = $(this).data('percentages');
			if(divid == 'chart1'){
				percentages = chart1data;
				var totalval = puavg;
			}
			if(divid == 'chart2'){
				percentages = chart2data;
			}			
			if(divid == 'chart3'){
				percentages = chart3data;				
				var totalval = pbavg;
			}
			if(divid == 'chart4'){
				percentages = chart4data;
			}	

			var percentagesarr = percentages.split(",");
			var percentagesarr = $.map(percentages.split(','), function(value){
				return parseInt(value, 10);
				// or return +value; which handles float values as well
			});
			var labels = $(this).data('labels');
			var labelsarr = labels.split(",");
			var colors = $(this).data('colors');
			var colorsarr = colors.split(",");
			
			
			var icon =$(this).data('icon');
			var labelcolors = $(this).data('labelcolors');
			var labelcolorsarr = labelcolors.split(",");
			var sessionChartoptions = {
				chart: {
					type: 'donut',
					width: '120%',
					animations: {
						enabled: true,
						easing: 'easeinout',
						speed: 800,
						animateGradually: {
							enabled: true,
							delay: 150
						},
						dynamicAnimation: {
							enabled: true,
							speed: 350
						}
					}
				},
				dataLabels: {
					enabled: true,
					textAnchor: 'end',
					formatter: function (val) {
						var newval = Math.round(val);
						return newval + "%"
					}
				},
				series: percentagesarr,
				legend: {
					show: false
				},
				labels: labelsarr,
				stroke: { width: 0 },
				colors: colorsarr,
				plotOptions: {
					pie: {
						customScale: 0.9,
						donut: {
							size: '55%',
							labels: {
								show: true,
								name: {
										show: false
									},
								value: {
										show: true,
										fontFamily: 'Assistant',
										color:'#4A4A4A',
										fontSize: "28px",
										formatter: function (val) {
											var newval = Math.round(val);
											return newval
										}
									},
									icon: {
										show: true,
										fontFamily: 'FontAwesome',
										color:'#4A4A4A',
										fontSize: "28px",
										icon: icon
									},
									total: {
										show: true,
										showAlways: true,
										fontFamily: 'Assistant',
										color:'#4A4A4A',
										fontSize: "28px",
										formatter: function (w) {
											if(divid == 'chart1' || divid == 'chart3'){
													return w.globals.seriesTotals.reduce((a, b) => {
														return Math.round(totalval)
													}, 0)
												}else{
													return ''
												}
										}
									}
							}
						},
					}
				}
			}
			var sessionChart = new ApexCharts(
				document.querySelector("#"+divid),
				sessionChartoptions
			);
			sessionChart.render();

		});

		var $dark_green = '#F56323';
		var $green = '#FBC02D';
		var $light_green = '#7ED321';

		var $primary = '#7367F0';
		var $success = '#28C76F';
		var $danger = '#EA5455';
		var $warning = '#FF9F43';
		var $label_color = '#1E1E1E';
		var grid_line_color = '#dae1e7';
		var scatter_grid_color = '#f3f3f3';
		var $scatter_point_light = '#D1D4DB';
		var $scatter_point_dark = '#5175E0';
		var $white = '#fff';
		var $black = '#000';

		var themeColors = [$light_green, $green, $dark_green];

		

		var ctx = document.getElementById("groupChart1").getContext("2d");
		var male = chart5datam;
		var malearr = male.split(",");
		var female = chart5dataf;
		var femalearr = female.split(",");
		var data = {
			labels: ["5-25 Years", "25-35 Years", "35-44 Years", "45-55 Years", "55-65 Years", "Above 65 Years"],
			datasets: [{
			label: "Male",
			backgroundColor: "#748789",
			data: malearr
			}, {
			label: "Female",
			backgroundColor: "#58b6c0",
			data: femalearr
			}]
		};
		
		myBarChart1 = new Chart(ctx, {
			type: 'bar',
			data: data,
			grouped: true,
			options: {
			barValueSpacing: 100,
			scales: {
				yAxes: [{
				ticks: {
					beginAtZero: true,
					min: 0,
				}
				}]
			}
			}
		});

		var ctx = document.getElementById("groupChart2").getContext("2d");
		var male = chart6datam;
		var malearr = male.split(",");
		var female = chart6dataf;
		var femalearr = female.split(",");
		var data = {
			labels: ["5-25 Years", "25-35 Years", "35-44 Years", "45-55 Years", "55-65 Years", "Above 65 Years"],
			datasets: [{
			label: "Male",
			backgroundColor: "#748789",
			data: malearr
			}, {
			label: "Female",
			backgroundColor: "#58b6c0",
			data: femalearr
			}]
		};
		
		myBarChart2 = new Chart(ctx, {
			type: 'bar',
			data: data,
			grouped: true,
			options: {
			barValueSpacing: 100,
			scales: {
				yAxes: [{
				ticks: {
					beginAtZero: true,
					min: 0,
				}
				}]
			}
			}
		});
		$(window).resize();
		$(window).trigger('resize');
		if(newdata != 'no'){
			var scbuyers =  filterJson(jsondata,'sc');
			var hpbuyers =  filterJson(jsondata,'hp');
			var interest = (parseInt(scbuyers.length)/parseInt(totalbuyers))*100;
			var hesitate = (parseInt(hpbuyers.length)/parseInt(scbuyers.length))*100;
			var desire = (parseInt(totalpubuyers)/parseInt(scbuyers.length))*100;
			
			var notpbbuyers = totalbuyers-totalpbbuyers;			
			var ccratioval = (parseInt(notpbbuyers)/parseInt(totalbuyers))*100;
			ccratioval = Math.round(ccratioval);
			$('.ccratio').html(ccratioval+'%');
			$('.ccratio').css('width',ccratioval+'%');
			$('.ccratio').attr('aria-valuenow',ccratioval);
			$('.ccratio').attr('aria-valuemin',ccratioval);			

			var buy = ((parseInt(totalpubuyers)-parseInt(totalpbbuyers))/parseInt(scbuyers.length))*100;
			var leaving = (parseInt(totalpbbuyers)/parseInt(scbuyers.length))*100;

/*
			var totalfunnel = interest+hesitate+desire+buy+leaving;
			interest = (interest/totalfunnel)*100;
			hesitate = (hesitate/totalfunnel)*100;
			desire = (desire/totalfunnel)*100;
			buy = (buy/totalfunnel)*100;
			leaving = (leaving/totalfunnel)*100;*/
			var dataarr = {};
			dataarr['Interest'] = Math.round(interest);
			dataarr['Hesitate'] = Math.round(hesitate);
			dataarr['Desire'] = Math.round(desire);
			dataarr['Buy'] = Math.round(buy);
			dataarr['Leaving'] = Math.round(leaving);
			var sortable = [];
			for (var arr in dataarr) {
				sortable.push([arr, dataarr[arr]]);
			}
			sortable.sort(function(a, b) {
				return b[1] - a[1];
			});
			dataarr = [];
			sortable.forEach(sortvalue=>{
				datavalue = {};
				datavalue['name'] = sortvalue[0];
				datavalue['value'] = sortvalue[1];
				dataarr.push(datavalue);
			})
				
			am4core.ready(function() {
				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end
		
				var chart = am4core.create("chartdiv", am4charts.SlicedChart);
				chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect
		
				chart.data = dataarr;
		
				var series = chart.series.push(new am4charts.FunnelSeries());
				series.colors.step = 2;
				series.dataFields.value = "value";
				series.dataFields.category = "name";
				series.alignLabels = true;
				series.fontSize = 24;
				series.fontWeight = 'bold';
		
				series.labelsContainer.paddingLeft = 15;
				series.labelsContainer.width = 250;
				series.labels = false;

				series.labels.template.adapter.add("text", slicePercent);
				series.tooltip.label.adapter.add("text", slicePercent);

				function slicePercent(text, target) {
				var max = target.dataItem.values.value.value - target.dataItem.values.value.startChange;
				var percent = Math.round(target.dataItem.values.value.value / max * 100);
				return "{category}: " + target.dataItem.values.value.value  + "%";
				}
		
				//series.orientation = "horizontal";
				//series.bottomRatio = 1;
				/*
				chart.legend = new am4charts.Legend();
				chart.legend.position = "left";
				chart.legend.valign = "bottom";
				chart.legend.margin(5,5,20,5);*/
		
			}); // end am4core.ready()



			// Bar Chart
		// ------------------------------------------
	
		//Get the context of the Chart canvas element we want to select
		var barChartctx = $("#bar-chart");
		var barChartctxbar = document.getElementById("bar-chart").getContext("2d");

		// Chart Data
		var barchartData = {
			labels: ["חלקי בשר כבש טרי","תה/קפה קר אישי","מוצרי חשמל קטנים","זיתים מגולענים","מעדני סויה","סוכריות טופי","טחון מן הצומח","פטרוזיליה","שמרים","סכיני גילוח לנשים","מגבונים להסרת איפור","מגבות","קשיו","קורונה","בשר משומר","מוצצים","סוכריות ילדים/לקקנים","מברשת שיניים חשמלית","בצל ירוק"],	
			datasets: [{
				data: ['-64.43','-63.29','-51.98','-44.29','-43.12','-39.02','-36.99','-34.28',	'-32.24','36.83','39.55','42.04','42.54','43.45','44.68','54.25','55.04','66.54','189.23'],
				backgroundColor: ['#ff0000','#ff0000','#ff0000','#ff0000','#ff0000','#ff0000','#ff0000','#ff0000','#ff0000','#38761D','#38761D','#38761D','#38761D','#38761D','#38761D','#38761D','#38761D','#38761D','#38761D'],
				borderColor: "transparent"
			}]
		};
		var barChartconfig = {
			type: 'bar',
			data: barchartData,
			options: {
				legend: {
					display: false,
				},
				scales: {
					y: {
						ticks: {
							callback: function(value, index, values) {
								return value + '%';
							}
						}
					}
				},			
			  	elements: {
					bar: {
				  		borderWidth: 2,
					}
			  	},
			  	responsive: true,
			  	plugins: {
					legend: {
						display: false,
					},
					title: {
				  		display: false
					}
			  	}
			},
		};

	
		// Create the chart
		var barChart = new Chart(barChartctxbar, barChartconfig);
		var barchartData2 = {
			labels: ["קורולציה/אחוז"],	
			datasets: [{
				data: ['0.3032'],
				backgroundColor: ['#FF9900'],
				borderColor: "transparent"
			}]
		};
		var barChartconfig2 = {
			type: 'bar',
			data: barchartData2,
			options: {
				legend: {
					display: false,
				},
			  	indexAxis: 'y',
				scales: {
					x: {						
						min: -0.2,
						ticks:{
							stepSize: 0.2
						}
					}
				},
			  	elements: {
					bar: {
				  		borderWidth: 2,
					}
			  	},
			  	responsive: true,
			  	plugins: {
					legend: {
						display: false,
					},
					title: {
				  		display: false
					}
			  	}
			},
		};

		var barChartctxbar2 = document.getElementById("bar-chart2").getContext("2d");
		var barChart2 = new Chart(barChartctxbar2, barChartconfig2);

		var barchartData3 = {
			labels: ["פיל - טונה","סטארקיסט","פוסידון","וילי פוד","ויליגר","אחר"],	
			datasets: [{
				data: ['-10.86','-0.54','21.70','17.89','-5.37','-0.71'],
				backgroundColor: ['#4F81BD'],
				borderColor: "transparent"
			}]
		};
		
		var barChartconfig3 = {
			type: 'bar',
			data: barchartData3,
			options: {
				legend: {
					display: false,
				},
			  	indexAxis: 'y',
				scales: {					
					x: {
						title: {
							display: true,
							text: 'חום/קורולציה/ אחוז '
						},
						min: -20,
						ticks:{
							stepSize: 10,
							callback: function(value, index, values) {
								return value + '%';
							}
						}
					},
					y: {
						title: {
							display: true,
							text: 'מותג'
						}
					}
				},
			  	elements: {
					bar: {
				  		borderWidth: 2,
					}
			  	},
			  	responsive: true,
			  	plugins: {
					legend: {
						display: false,
					},
					title: {
				  		display: false
					}
			  	}
			},
		};

		var barChartctxbar3 = document.getElementById("bar-chart3").getContext("2d");
		var barChart3 = new Chart(barChartctxbar3, barChartconfig3);


		var barchartData4 = {
			labels: ["מארזי טונה בשמן","מארזי טונה במים","טונה בודד","מארזי טונה בשמן זית"],	
			datasets: [{
				data: ['-3.72','16.52','1.00','10.22'],
				backgroundColor: ['#4F81BD','#FF9900','#D9D9D9','#FF9900'],
				borderColor: "transparent"
			}]
		};
		var barChartconfig4 = {
			type: 'bar',
			data: barchartData4,
			options: {
				legend: {
					display: false,
				},
			  	indexAxis: 'y',
				scales: {					
					x: {
						title: {
							display: true,
							text: 'חום                                                                                                                                      קור'
						},
						min: -5,
						ticks:{
							stepSize: 5,
							callback: function(value, index, values) {
								return value + '%';
							}
						}
					},
					y: {
						title: {
							display: true,
							text: 'סגמנט'
						}
					}
				},
			  	elements: {
					bar: {
				  		borderWidth: 2,
					}
			  	},
			  	responsive: true,
			  	plugins: {
					legend: {
						display: false,
					},
					title: {
				  		display: false
					}
			  	}
			},
		};

		var barChartctxbar4 = document.getElementById("bar-chart4").getContext("2d");
		var barChart4 = new Chart(barChartctxbar4, barChartconfig4);
		}

		
	}
	$('.owl-carousel').owlCarousel({
		rtl:true,
		nav:true,
		navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
	});
	  $('.tabref').on('click', function(e){
		var activetab = $(this).data('tabid');
		$('.tabcontent').fadeOut();		
		myBarChart1.destroy();
		myBarChart2.destroy();
		$('.menu-toggle').trigger('click');	
		$('#tab'+activetab+'data').fadeIn(function(){			
			setTimeout(function(){ updateCharts(jsondata,'no'); }, 500);
		});			
		e.preventDefault();
	  });
	  $('.openbigimg').on('click', function(e){
		var src = $(this).data('src');
		$('#bigimg').attr('src',src);
		e.preventDefault();
	  });

	

});
