<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use DateTime;

class DashboardController extends Controller
{
	public function formatPercent($data){
		return round($data);
	}
    // Dashboard - Analytics
    public function dashboardAnalytics(Request $request){
		if(!$_POST){
			return redirect('/search-information');
		}else{
			$pageConfigs = ['pageHeader' => false];
			$regions = DB::table("observations")->select("local")->distinct("local")->get();
			$brands = DB::table("observations")->select("brand")->distinct("brand")->get();
			$products = DB::table("observations")->select("product")->distinct("product")->get();
			$local = isset($_POST['local'])?$_POST['local']:'';
			$from = isset($_POST['fromdate_submit'])?$_POST['fromdate_submit']:'';
			$to = isset($_POST['todate_submit'])?$_POST['todate_submit']:'';
			$fromdate = isset($_POST['fromdate'])?$_POST['fromdate']:'';
			$todate = isset($_POST['todate'])?$_POST['todate']:'';
			$dp = isset($_POST['dp'])?$_POST['dp']:'';
			$category = isset($_POST['category'])?$_POST['category']:'';
			$brand = isset($_POST['brand'])?$_POST['brand']:'';
			$product = isset($_POST['product'])?$_POST['product']:'';
			$onlycategory = 'no';
			$brandarray = array();		
			$brandarray['פיל - טונה'] = 'filtuna';
			$brandarray['סטארקיסט'] = 'starkist';
			$brandarray['פוסידון'] = 'posidon';
			$brandarray['וילי פוד'] = 'villifood';
			$brandarray['ויליגר'] = 'villiger';
			$brandarray['ריאו'] = 'rio';
			$brandarray['טונה - אין מותג'] = 'noname';
			$brandarray['אחר'] = 'other';
			$brandarray['All Brands'] = 'all';
			if($brand != ''){
				$brandslug = $brandarray[$brand];
			}else{
				$brandslug = 'all';
				$brand = 'All Brands';
				$onlycategory = 'yes';
			}
			$noresults = 0;


			$mergeDashboardJSON = file_get_contents(base_path('resources/json/merge_dashboard.json'));
			$mDArr = json_decode($mergeDashboardJSON);

			/* Average Queries */
			
			$recommendations = DB::table('recommendations')->where('category', $category)->orderBy('icon','DESC')->get();
			$dynamicRows = DB::table('dynamic_texts')->get();
			$tootltips = array();
			if(count($dynamicRows) > 0){
				foreach($dynamicRows as $key=>$value){
					for($j=1;$j<=16;$j++){
						$tooltip = 'tooltip'.$j;
						$tootltips[] = $value->{$tooltip};
					}
				}
			}
			if($brand != 'All Brands'){
				$insights = DB::table('insights')->where('brand', $brand)->get();
			}else{
				$insights = DB::table('insights')->where('brand', NULL)->get();
			}
			$buyerinsights = array();
			$funnelinsights = array();
			$saleinsights = array();
			foreach($insights as $insight){
				$showinarr = explode(',',$insight->showin);
				if (in_array("Buyers", $showinarr)){
					$buyerinsights[] = $insight;
				}
				if (in_array("Funnel", $showinarr)){
					$funnelinsights[] = $insight;
				}
				if (in_array("Sales", $showinarr)){
					$saleinsights[] = $insight;
				}
			}		
			return view('/pages/dashboard-analytics', [
				'pageConfigs' => $pageConfigs,
				'noresults' => $noresults,
				'regions' => $regions,
				'brands' => $brands,
				'products' => $products,
				'local' => $local,
				'from' => $from,
				'to' => $to,
				'fromdate' => $fromdate,
				'todate' => $todate,
				'dp' => $dp,
				'category' => $category,
				'brand' => $brand,
				'brandslug' => $brandslug,
				'onlycategory' => $onlycategory,
				'product' => $product,
				'data' => implode(',',$_POST),
				'jsondata' => json_encode($_POST),
				'inscount' => count($insights),
				'insights' => $insights,
				'buyerinsights' => $buyerinsights,
				'funnelinsights' => $funnelinsights,
				'saleinsights' => $saleinsights,
				'tootltips' => $tootltips	
			]);
		}
    }
	
    // Dashboard - Ecommerce
    public function dashboardEcommerce(){
        $pageConfigs = [
            'pageHeader' => false
        ];

        return view('/pages/dashboard-ecommerce', [
            'pageConfigs' => $pageConfigs
        ]);
    }
	public function Comparisons(){
        $pageConfigs = [
            'pageHeader' => false
        ];

        return view('/pages/comparisons', [
            'pageConfigs' => $pageConfigs
        ]);
    }
}
