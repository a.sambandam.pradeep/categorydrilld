<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ExtensionController extends Controller
{
    // Sweet Alert
    public function sweet_alert(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Sweet Alert"]
      ];
      return view('/pages/ext-component-sweet-alerts', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Toastr
    public function toastr(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Toastr"]
      ];
      return view('/pages/ext-component-toastr', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // NoUi Slider
    public function noui_slider(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"NoUi Slider"]
      ];
      return view('/pages/ext-component-noui-slider', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }
	

    // File Uploader
    public function file_uploader(){		
        $breadcrumbs = [
            ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"File Uploader"]
        ];
        return view('/pages/ext-component-file-uploader', [
            'breadcrumbs' => $breadcrumbs
        ]);
    }
	public function csv_uploader(Request $request) {
        if (!empty($_FILES)) {
            $ds          = '/';  //1
            $storeFolder = public_path().'/json';   //2
     
            $tempFile = $_FILES['file']['tmp_name'];          //3             
              
            $targetPath = $storeFolder . $ds;  //4
             
            $targetFile =  $targetPath. $_FILES['file']['name'];  //5
         
            move_uploaded_file($tempFile,$targetFile); //6
             
        }
        /*
		if ( isset( $_FILES['file']['tmp_name'] ) ){
	
			$file_name = $_FILES['file']['tmp_name'];
			
			if( file_exists( $file_name ) ){
				
				$file = fopen( $file_name, 'r' );
				$i=1;
				while ($f = fgetcsv($file, 1024, ',')) {
					if($i != 1){
						if($f[0] != '' &&
						   $f[1] != '' &&
						   $f[2] != '' &&
						   $f[3] != '' &&
						   $f[4] != '' &&
						   $f[5] != '' &&
						   $f[6] != '' &&
						   //$f[7] != '' &&
						   $f[8] != '' &&
						   $f[9] != '' &&
						   $f[10] != '' &&
						   $f[11] != '' &&
						   $f[12] != '' &&
						   $f[13] != '' &&
						   $f[14] != '' &&
						   $f[15] != '' &&
						   $f[16] != '' &&
						   $f[17] != '' &&
						   $f[18] != '' &&
						   $f[19] != '' &&
						   $f[20] != '' &&
						   $f[21] != ''
						  ){
							$your_date = date("Y-m-d", strtotime($f[4]));
							DB::table('observations')->insert(
								[
									'id_p' => $f[1], 
									'local' => $f[2],
									'camera' => $f[3],
									'date' => $your_date,
									'hour' => $f[5],
									'composition' => $f[6],
									'uniform' => $f[7],
									'age' => $f[8],
									'gender' => $f[9],
									'timeline_dp' => $f[10],
									'timeline_c' => $f[11],
									'dp' => $f[12],
									'category' => $f[13],
									'brand' => $f[14],
									'product' => $f[15],
									'timefind_pro' => $f[16],
									'decision_pro' => $f[17],
									'interaction' => $f[18],
									'cart' => $f[19],
									'count' => $f[20],
									'shelf' => $f[21],
									'TypeInteraction' => $f[22],
									'NumberInteractions' => $f[23],
									'TimeDesicion' => $f[24],
									'ScoreTimeDesicion' => $f[25],
									'TimeFind' => $f[26],
									'ScoreTimeFind' => $f[27],
									'ImpulseScore' => $f[28],
									'ImpulseScoreForCart' => $f[29]
								]
							);
						}
					}
					$i++;
				}
				
			}

		}
		*/
	}
	public function cleanup_db(){
		DB::table('observations')->truncate();
		return redirect(route('route.home'));
	}
    // Quill Editor
    public function quill_editor(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Quill Editor"]
      ];
      return view('/pages/ext-component-quill-editor', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Drag Drop
    public function drag_drop(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Drag & Drop"]
      ];
      return view('/pages/ext-component-drag-drop', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Tour
    public function tour(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Tour"]
      ];
      return view('/pages/ext-component-tour', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Clipboard
    public function clipboard(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Clipboard"]
      ];
      return view('/pages/ext-component-clipboard', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Media Player
    public function plyr(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Media player"]
      ];
      return view('/pages/ext-component-plyr', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Context Menu
    public function context_menu(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Context Menu"]
      ];
      return view('/pages/ext-component-context-menu', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // I18n
    public function i18n(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"I18n"]
      ];
      return view('/pages/ext-component-i18n', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    public function swiper(){
        $breadcrumbs = [
            ['link'=>"dashboard-analytics",'name'=>"Home"],['link'=>"dashboard-analytics",'name'=>"Extensions"], ['name'=>"Swiper"]
        ];
        return view('/pages/ext-component-swiper', [
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
