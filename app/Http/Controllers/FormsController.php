<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class FormsController extends Controller
{
    // Form Elements - Select
    public function select(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Select"]
      ];
      return view('/pages/form-select', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Elements - Switch
    public function switch(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Switch"]
      ];
      return view('/pages/form-switch', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Elements - Checkbox
    public function checkbox(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Checkbox"]
      ];
      return view('/pages/form-checkbox', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Elements - Radio
    public function radio(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Radio"]
      ];
      return view('/pages/form-radio', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Elements - Input
    public function input(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Input"]
      ];
      return view('/pages/form-input', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Elements - Input-groups
    public function input_groups(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Input Groups"]
      ];
      return view('/pages/form-input-groups', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Elements - Number Input
    public function number_input(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Number Input"]
      ];
      return view('/pages/form-number-input', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Elements - Textarea
    public function textarea(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Textarea"]
      ];
      return view('/pages/form-textarea', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Elements - Date & time Picker
    public function date_time_picker(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Form Elements"], ['name'=>"Date & Time Picker"]
      ];
      return view('/pages/form-date-time-picker', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Layouts
    public function searchinformation(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Forms"], ['name'=>"Form Layouts"]
      ];
	  $user = Auth::user();
      return view('/pages/searchinformation', [
          'breadcrumbs' => $breadcrumbs,
		  'user' => $user
      ]);
    }
	
	public function updatetext(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Forms"], ['name'=>"Form Layouts"]
      ];
      $user = Auth::user();
      $rows = DB::table('recommendations')->get();
      $insights = DB::table('insights')->get();
      $categories = DB::table("observations")->select("category")->distinct("category")->get();
      $dynamicRows = DB::table('dynamic_texts')->get();
      $request = [];
      $ic = 1;
      $request['recommendation1'] = '';
      $request['recommendationButton1'] = '';
      $request['notes1'] = '';
      $request['category'] = '';
      $request['icon'] = '';
      $request['id'] = '';
      $request['buttonText']='Submit';
      for($j=1;$j<=16;$j++){
        $request['tooltip'.$j] = '';
      }
      if(count($dynamicRows) > 0){
        foreach($dynamicRows as $key=>$value){
      for($j=1;$j<=16;$j++){
        $tooltip = 'tooltip'.$j;
        $request['tooltip'.$j] = $value->{$tooltip};
      }
        }
        $request['post_type']='update';
      }
      $tttextarr = array('Buyer Profile Avg Age', 'Buyer Profile Gender Segmentation', 'Buyer Profile Demographic segmentation', 'Potential Buyer Avg age', 'Potential Buyer Gender segmentation', 'Potential Buyer Demographic segmentation', 'In-store audience involvement', 'Product abandonment', 'Category conversion ratio', 'Products that encourage impulse buying', 'נטישה במהלך רכישה', 'נטישה למוצר
      ', 'יחס ההמרה מהקטגוריה', 'מדד ה IMPULSE BUYING', 'מוצרים משלימים', 'תובנות והמלצות');
    
      return view('/pages/updatetext', [
          'breadcrumbs' => $breadcrumbs,
          'user' => $user,
          'request' => $request,
          'insights' => $insights,
          'rows' => $rows,
          'categories' => $categories,
          'tttextarr' => $tttextarr
      ]);
    }
    public function updatetextdb(Request $request){        
        $post_type=$request->get('post_type');
        if($post_type === 'recommendation') {
          $recommendation1 = $request->get('recommendation1');
          $recommendationButton1 = $request->get('recommendationButton1');
          $category = $request->get('category');
          $icon = $request->get('icon');
          $notes1 = $request->get('notes1');
          $id=$request->get('rec_id');
          DB::table('recommendations')->updateOrInsert(
            ['id'=>$id],
            [
                'recommendation' => $recommendation1,
                'recommendationButton' => $recommendationButton1,
                'category' => $category,
                'icon' => $icon,
                'notes' => $notes1
            ]);
        }
          
        if($post_type === 'insight') {
          $title = $request->get('title');
          $icon = $request->file('icon');
          $iconname = '';
          if($icon){
            $iconname = time().$icon->getClientOriginalName();  
            $icon->move('uploads',$iconname);  
          }
          $maintext = $request->get('maintext');
          $datetext = $request->get('datetext');
          $category = $request->get('category');
          $brand = $request->get('brand');
          $showin = $request->get('showin');
          if($showin){
            $showin = implode(',',$showin);
          }
          $id=$request->get('ins_id');
          if($iconname != ''){
            DB::table('insights')->updateOrInsert(
              ['id'=>$id],
              [
                  'title' => $title,
                  'icon' => $iconname,
                  'maintext' => $maintext,
                  'datetext' => $datetext,
                  'category' => $category,
                  'brand' => $brand,
                  'showin' => $showin
              ]);
            }else{
              DB::table('insights')->updateOrInsert(
                ['id'=>$id],
                [
                    'title' => $title,
                    'maintext' => $maintext,
                    'datetext' => $datetext,
                    'category' => $category,
                    'brand' => $brand,
                    'showin' => $showin
                ]);
            }
        }
        
        return redirect(route('route-updateText'));
		
    }
    public function updatetooltips(Request $request) {
      $tooltip1 = $request->get('tooltip1');
      $tooltip2 = $request->get('tooltip2');
      $tooltip3 = $request->get('tooltip3');
      $tooltip4 = $request->get('tooltip4');
      $tooltip5 = $request->get('tooltip5');
      $tooltip6 = $request->get('tooltip6');
      $tooltip7 = $request->get('tooltip7');
      $tooltip8 = $request->get('tooltip8');
      $tooltip9 = $request->get('tooltip9');
      $tooltip10 = $request->get('tooltip10');
      $tooltip11 = $request->get('tooltip11');
      $tooltip12 = $request->get('tooltip12');
      $tooltip13 = $request->get('tooltip13');
      $tooltip14 = $request->get('tooltip14');
      $tooltip15 = $request->get('tooltip15');
      $tooltip16 = $request->get('tooltip16');
      DB::table('dynamic_texts')->updateOrInsert(
        ['id'=>1],
        [
                'tooltip1' => $tooltip1,
                'tooltip2' => $tooltip2,
                'tooltip3' => $tooltip3,
                'tooltip4' => $tooltip4,
                'tooltip5' => $tooltip5,
                'tooltip6' => $tooltip6,
                'tooltip7' => $tooltip7,
                'tooltip8' => $tooltip8,
                'tooltip9' => $tooltip9,
                'tooltip10' => $tooltip10,
                'tooltip11' => $tooltip11,
                'tooltip12' => $tooltip12,
                'tooltip13' => $tooltip13,
                'tooltip14' => $tooltip14,
                'tooltip15' => $tooltip15,
                'tooltip16' => $tooltip16
        ]);
  

return redirect(route('route.home'));
    }
    public function deleteRecommendation(Request $request) {
      $id=$request->get('id');
      DB::table('recommendations')->where('id', $id)->delete();
    }
    public function deleteInsight(Request $request) {
      $id=$request->get('id');
      DB::table('insights')->where('id', $id)->delete();
    }
	public function get_dps(Request $request){
		$select = $request->get('select');
		$value = $request->get('value');
		$dependent = $request->get('dependent');
		$local = $request->get('local');
		$dp = $request->get('dp');
		$category = $request->get('category');
    $brand = $request->get('brand');
    $fromdate =  $request->get('fromdate');
    $fromdate = str_replace('/', '-', $fromdate);
    $from = date("Y-m-d", strtotime($fromdate) );
    $todate =  $request->get('todate');
    $todate = str_replace('/', '-', $todate);
    $to = date("Y-m-d", strtotime($todate) );
    
    //DB::enableQueryLog();
		if($dependent == 'dp'){
			$data = DB::table('observations')->select($dependent)->where($select, $value)->whereBetween('date', [$from, $to])->distinct($dependent)->get();
		}
		if($dependent == 'category'){
			$data = DB::table('observations')->select($dependent)->where('local', $local)->whereBetween('date', [$from, $to])->where($select, $value)->distinct($dependent)->get();
		}
		if($dependent == 'brand'){
			$data = DB::table('observations')->select($dependent)->where('local', $local)->whereBetween('date', [$from, $to])->where('dp', $dp)->where($select, $value)->distinct($dependent)->get();
		}
		if($dependent == 'product'){
			$data = DB::table('observations')->select($dependent)->where('local', $local)->whereBetween('date', [$from, $to])->where('dp', $dp)->where('category', $category)->where($select, $value)->distinct($dependent)->get();
		}
		$output='';
		foreach($data as $row)
		{
			$output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
    }
    //dd(DB::getQueryLog());
		echo $output;
	}
	
    // Form Wizard
    public function wizard(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Forms"], ['name'=>"Wizard"]
      ];
      return view('/pages/form-wizard', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }

    // Form Validation
    public function validation(){
      $breadcrumbs = [
          ['link'=>"dashboard-analytics",'name'=>"Home"], ['link'=>"dashboard-analytics",'name'=>"Forms"], ['name'=>"Validation"]
      ];
      return view('/pages/form-validation', [
          'breadcrumbs' => $breadcrumbs
      ]);
    }
}
